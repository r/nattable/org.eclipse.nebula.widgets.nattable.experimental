package org.eclipse.nebula.widgets.nattable.core.layer.axis.impl

import org.junit.Before
import org.junit.Test

import static org.eclipse.nebula.widgets.nattable.core.layer.AxisTest.*

class ResizeAxisTest {
	
	var AxisImpl axis
	
	@Before
	def void before() {
		axis = new AxisImpl(4, 100)
	}
	
	@Test
	def void baseline() {
		testAxis(
			axis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void resize() {
		axis.setPixelSizeOfSegmentPosition(25, 1)
		
		testAxis(
			axis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 125.0, 225.0, 325.0 ]
		)
	}
	
}