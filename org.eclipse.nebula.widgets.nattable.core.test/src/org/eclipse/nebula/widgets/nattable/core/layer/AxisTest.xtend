package org.eclipse.nebula.widgets.nattable.core.layer

import java.io.Serializable
import java.util.List
import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis

import static org.junit.Assert.*

import static extension org.eclipse.nebula.widgets.nattable.core.layer.axis.AxisInvariants.*

class AxisTest {
	
	def static testAxis(Axis axis, List<? extends Serializable> expectedIds, List<Double> expectedStartPixels) {
		testAxis(axis, expectedIds, expectedStartPixels, expectedStartPixels, null)
	}
	
	def static testAxis(Axis axis, List<? extends Serializable> expectedIds, List<Double> expectedStartPixels, List<Double> expectedOriginPixels, List<Double> expectedPixelSizes) {
		testAxis("", axis, expectedIds, expectedStartPixels, expectedOriginPixels, expectedPixelSizes)
	}
	
	def static testAxis(String axisName, Axis axis, List<? extends Serializable> expectedIds, List<Double> expectedStartPixels) {
		testAxis(axisName, axis, expectedIds, expectedStartPixels, expectedStartPixels, null)
	}
	
	def static testAxis(String axisName, Axis axis, List<? extends Serializable> expectedIds, List<Double> expectedStartPixels, List<Double> expectedOriginPixels, List<Double> expectedPixelSizes) {
		// Axis segment count
		assertEquals('''«axisName» getSegmentCount''', expectedIds.size, axis.segmentCount)
		
		// Start pixel of segment position
		for (segmentPosition : 0 .. expectedIds.size)
			assertEquals('''«axisName» getStartPixelOfSegmentPosition(«segmentPosition»)''', expectedStartPixels.get(segmentPosition), axis.getStartPixelOfSegmentPosition(segmentPosition), 0)
		
		// Origin pixel of segment position
		for (segmentPosition : 0 ..< expectedIds.size)
			assertEquals('''«axisName» getOriginPixelOfSegmentPosition(«segmentPosition»)''', expectedOriginPixels.get(segmentPosition), axis.getOriginPixelOfSegmentPosition(segmentPosition), 0)
		
		// Pixel size of segment position
		if (expectedPixelSizes != null)
			for (segmentPosition : 0 ..< expectedIds.size)
				assertEquals('''«axisName» getPixelSizeOfSegmentPosition(«segmentPosition»)''', expectedPixelSizes.get(segmentPosition), axis.getPixelSizeOfSegmentPosition(segmentPosition), 0)
		
		// Segment position of pixel location
		assertEquals('''«axisName» < range getSegmentPositionOfPixelLocation(«expectedOriginPixels.get(0) - 1»)''', -1, axis.getSegmentPositionOfPixelLocation(expectedOriginPixels.get(0) - 1))
		for (segmentPosition : 0 ..< expectedIds.size) {
			val pixelLocation = expectedStartPixels.get(segmentPosition)
			assertEquals('''«axisName» getSegmentPositionOfPixelLocation(«pixelLocation»)''', segmentPosition, axis.getSegmentPositionOfPixelLocation(pixelLocation))
		}
		assertEquals('''«axisName» > range getSegmentPositionOfPixelLocation(«axis.pixelSize»)''', axis.segmentCount, axis.getSegmentPositionOfPixelLocation(axis.pixelSize))
		
		// Id of segment position
		for (segmentPosition : 0 ..< expectedIds.size)
			assertEquals('''«axisName» getIdOfSegmentPosition(«segmentPosition»)''', expectedIds.get(segmentPosition), axis.getIdOfSegmentPosition(segmentPosition))

		// Segment position of id
		for (segmentId : expectedIds)
			assertEquals('''«axisName» getSegmentPositionOfId(«segmentId»)''', expectedIds.indexOf(segmentId), axis.getSegmentPositionOfId(segmentId))
		
		// Axis pixel size
		assertEquals('''«axisName» pixelSize''', expectedStartPixels.get(expectedStartPixels.size - 1) - expectedStartPixels.get(0), axis.pixelSize, 0)
	}
	
}