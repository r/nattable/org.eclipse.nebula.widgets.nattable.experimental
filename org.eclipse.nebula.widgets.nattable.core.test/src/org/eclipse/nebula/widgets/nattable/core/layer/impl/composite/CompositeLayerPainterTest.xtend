package org.eclipse.nebula.widgets.nattable.core.layer.impl.composite

import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.core.graphics.GraphicsContext
import org.eclipse.nebula.widgets.nattable.core.graphics.LayerPainterFactory
import org.eclipse.nebula.widgets.nattable.core.layer.LayerPainter
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DimensionallyDependentLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DummyLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.LayerDataAccessorImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.ColumnHeaderLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.RowHeaderLayer
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner

import static org.junit.Assert.*
import static org.mockito.Mockito.*

import static extension org.eclipse.nebula.widgets.nattable.core.layer.LayerInvariants.*

@RunWith(typeof(MockitoJUnitRunner))
class CompositeLayerPainterTest {

	//  20  100   100   100   100
	// +--+-----+-----+-----+-----+
	// |  |     |     |     |     | 20
	// +--+-----+-----+-----+-----+
	// |  | 0,0 | 1,0 | 2,0 | 3,0 | 20
	// +--+-----+-----+-----+-----+
	// |  | 0,1 | 1,1 | 2,1 | 3,1 | 20
	// +--+-----+-----+-----+-----+
	// |  | 0,2 | 1,2 | 2,2 | 3,2 | 20
	// +--+-----+-----+-----+-----+
	DummyLayer bodyLayer = new DummyLayer(
			new AxisImpl(4, 100),  // Horizontal axis
			new AxisImpl(3, 20)  // Vertical axis
		)
	ColumnHeaderLayer columnHeaderLayer = new ColumnHeaderLayer(bodyLayer.horizontalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | columnId ]))
	RowHeaderLayer rowHeaderLayer = new RowHeaderLayer(bodyLayer.verticalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | rowId ]))
	DimensionallyDependentLayer cornerLayer = new DimensionallyDependentLayer(rowHeaderLayer.horizontalAxis, columnHeaderLayer.verticalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | "" ]))
	CompositeLayer compositeLayer = new CompositeLayer => [
		addRow(cornerLayer, columnHeaderLayer)
		addRow(rowHeaderLayer, bodyLayer)
	]
	
	@Mock LayerPainter<DummyLayer> bodyLayerPainter
	@Mock LayerPainter<ColumnHeaderLayer> columnHeaderLayerPainter
	@Mock LayerPainter<RowHeaderLayer> rowHeaderLayerPainter
	@Mock LayerPainter<DimensionallyDependentLayer> cornerLayerPainter
	
	@Mock LayerPainterFactory layerPainterFactory
	
	@InjectMocks CompositeLayerPainter layerPainter = new CompositeLayerPainter
	@Mock GraphicsContext testGC
	
	@Before
	def void before() {
		when(layerPainterFactory.getLayerPainter(bodyLayer)).thenReturn(bodyLayerPainter)
		when(layerPainterFactory.getLayerPainter(columnHeaderLayer)).thenReturn(columnHeaderLayerPainter)
		when(layerPainterFactory.getLayerPainter(rowHeaderLayer)).thenReturn(rowHeaderLayerPainter)
		when(layerPainterFactory.getLayerPainter(cornerLayer)).thenReturn(cornerLayerPainter)
	}
	
	@Test
	def void baseline() {
		assertEquals(5, compositeLayer.columnCount)
		assertEquals(4, compositeLayer.rowCount)
		assertEquals(new PixelArea(420, 80), compositeLayer.pixelArea)
	}
	
	@Test
	def void childPixelArea() {
		layerPainter.paintLayer(compositeLayer, compositeLayer.pixelArea, testGC)
		verify(cornerLayerPainter).paintLayer(cornerLayer, new PixelArea(20, 20), testGC)
		verify(columnHeaderLayerPainter).paintLayer(columnHeaderLayer, new PixelArea(400, 20), testGC)
		verify(rowHeaderLayerPainter).paintLayer(rowHeaderLayer, new PixelArea(20, 60), testGC)
		verify(bodyLayerPainter).paintLayer(bodyLayer, new PixelArea(400, 60), testGC)
	}
	
}