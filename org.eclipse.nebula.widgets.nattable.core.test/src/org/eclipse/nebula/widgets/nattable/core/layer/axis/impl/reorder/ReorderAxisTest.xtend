package org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.reorder

import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.junit.Before
import org.junit.Test

import static org.eclipse.nebula.widgets.nattable.core.layer.AxisTest.*

class ReorderSegmentPositionAxisTest {
	
	Axis underlyingAxis
	ReorderAxis reorderAxis
	
	@Before
	def void before() {
		underlyingAxis = new AxisImpl(4, 100)
		reorderAxis = new ReorderAxis(underlyingAxis)
	}
	
	@Test
	def void baseline() {
		testAxis(
			"underlyingAxis",
			underlyingAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
		
		testAxis(
			"reorderAxis",
			reorderAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void reorderFromBeginningToMiddle() {
		reorderAxis.reorderSegmentPosition(0, 2)
		
		testAxis(
			"reorderAxis",
			reorderAxis,
			#[ 1, 2, 0, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void reorderFromBeginningToEnd() {
		reorderAxis.reorderSegmentPosition(0, 3)
		
		testAxis(
			"reorderAxis",
			reorderAxis,
			#[ 1, 2, 3, 0 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void reorderFromMiddleToBeginning() {
		reorderAxis.reorderSegmentPosition(2, 0)
		
		testAxis(
			"reorderAxis",
			reorderAxis,
			#[ 2, 0, 1, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void reorderFromMiddleToMiddleLeftToRight() {
		reorderAxis.reorderSegmentPosition(1, 2)
		
		testAxis(
			"reorderAxis",
			reorderAxis,
			#[ 0, 2, 1, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void reorderFromMiddleToMiddleRightToLeft() {
		reorderAxis.reorderSegmentPosition(2, 1)
		
		testAxis(
			"reorderAxis",
			reorderAxis,
			#[ 0, 2, 1, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void reorderFromMiddleToEnd() {
		reorderAxis.reorderSegmentPosition(1, 3)
		
		testAxis(
			"reorderAxis",
			reorderAxis,
			#[ 0, 2, 3, 1 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void reorderFromEndToBeginning() {
		reorderAxis.reorderSegmentPosition(3, 0)
		
		testAxis(
			"reorderAxis",
			reorderAxis,
			#[ 3, 0, 1, 2 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void reorderFromEndToMiddle() {
		reorderAxis.reorderSegmentPosition(3, 2)
		
		testAxis(
			"reorderAxis",
			reorderAxis,
			#[ 0, 1, 3, 2 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
}