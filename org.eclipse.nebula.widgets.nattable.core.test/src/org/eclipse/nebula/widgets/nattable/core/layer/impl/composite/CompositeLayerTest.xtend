package org.eclipse.nebula.widgets.nattable.core.layer.impl.composite

import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DimensionallyDependentLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DummyLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.LayerDataAccessorImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.ColumnHeaderLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.RowHeaderLayer
import org.junit.Before
import org.junit.Test

import static org.eclipse.nebula.widgets.nattable.core.layer.AxisTest.*

class CompositeLayerTest {
	
	val columnHeaders = #{ "A", "B", "C", "D" }
	Layer cornerLayer
	Layer columnHeaderLayer
	Layer rowHeaderLayer
	Layer bodyLayer
	CompositeLayer compositeLayer
	
	@Before
	def void before() {
		bodyLayer = new DummyLayer(
			new AxisImpl(4, 200),
			new AxisImpl(4, 100)
		)
		columnHeaderLayer = new ColumnHeaderLayer(bodyLayer.horizontalAxis, new LayerDataAccessorImpl [ layer, columnId, rowId | columnHeaders.get(columnId as Integer) ])
		rowHeaderLayer = new RowHeaderLayer(bodyLayer.verticalAxis, new LayerDataAccessorImpl [ layer, columnId, rowId | rowId ])
		cornerLayer = new DimensionallyDependentLayer(rowHeaderLayer.horizontalAxis, columnHeaderLayer.verticalAxis, new LayerDataAccessorImpl [ layer, columnId, rowId | "" ])
		
		compositeLayer = new CompositeLayer => [
			addRow(cornerLayer, columnHeaderLayer)
			addRow(rowHeaderLayer, bodyLayer)
		]
	}
	
	@Test
	def void bodyLayer() {
		testAxis(
			"bodyLayer.horizontalAxis",
			bodyLayer.horizontalAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 200.0, 400.0, 600.0, 800.0 ]
		)
		
		testAxis(
			"bodyLayer.verticalAxis",
			bodyLayer.verticalAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void columnHeaderLayer() {	
		testAxis(
			"columnHeaderLayer.horizontalAxis",
			columnHeaderLayer.horizontalAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 200.0, 400.0, 600.0, 800.0 ]
		)
		
		testAxis(
			"columnHeaderLayer.verticalAxis",
			columnHeaderLayer.verticalAxis,
			#[ 0 ],
			#[ 0.0, 20.0 ]
		)
	}
	
	@Test
	def void rowHeaderLayer() {
		testAxis(
			"rowHeaderLayer.horizontalAxis",
			rowHeaderLayer.horizontalAxis,
			#[ 0 ],
			#[ 0.0, 20.0 ]
		)
		
		testAxis(
			"rowHeaderLayer.verticalAxis",
			rowHeaderLayer.verticalAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void cornerLayer() {
		testAxis(
			"cornerLayer.horizontalAxis",
			cornerLayer.horizontalAxis,
			#[ 0 ],
			#[ 0.0, 20.0 ]
		)
		
		testAxis(
			"cornerLayer.verticalAxis",
			cornerLayer.verticalAxis,
			#[ 0 ],
			#[ 0.0, 20.0 ]
		)
	}
	
	@Test
	def void compositeLayer() {
		testAxis(
			"compositeLayer.horizontalAxis",
			compositeLayer.horizontalAxis,
			#[ 0, 0, 1, 2, 3 ],
			#[ 0.0, 20.0, 220.0, 420.0, 620.0, 820.0 ]
		)
		
		testAxis(
			"compositeLayer.verticalAxis",
			compositeLayer.verticalAxis,
			#[ 0, 0, 1, 2, 3 ],
			#[ 0.0, 20.0, 120.0, 220.0, 320.0, 420.0 ]
		)
	}
	
}