package org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport

import org.eclipse.nebula.widgets.nattable.core.big.layer.axis.impl.NotSoBigAxis
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.junit.Before
import org.junit.Test

import static org.eclipse.nebula.widgets.nattable.core.layer.AxisTest.*

import static extension org.eclipse.nebula.widgets.nattable.core.layer.axis.AxisInvariants.*

class ViewportAxisTest {
	
	AxisImpl underlyingAxis
	ViewportAxis viewportAxis
	
	@Before
	def void before() {
		// segment position: | 0 |  1  |      2      |            3            |
		//   pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		underlyingAxis = new AxisImpl(4, 100) => [
			setPixelSizeOfSegmentPosition(2, 0)
			setPixelSizeOfSegmentPosition(3, 1)
			setPixelSizeOfSegmentPosition(7, 2)
			setPixelSizeOfSegmentPosition(13, 3)
		]
		viewportAxis = new ViewportAxis(new NotSoBigAxis(underlyingAxis), null)
	}
	
	@Test
	def void baseline() {
		// segment position: | 0 |  1  |      2      |            3            |
		//   pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		testAxis(
			"underlyingAxis",
			underlyingAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 2.0, 5.0, 12.0, 25.0 ]
		)
		
		testAxis(
			"viewportAxis",
			viewportAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 2.0, 5.0, 12.0, 25.0 ]
		)
		
		viewportAxis.visiblePixelSize = underlyingAxis.pixelSize
		// segment position: | 0 |  1  |      2      |            3            |
		//   pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		testAxis(
			"viewportAxis",
			viewportAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 2.0, 5.0, 12.0, 25.0 ]
		)
	}
	
	@Test
	def void beginningToBoundary() {
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size: |---------|
		viewportAxis.viewportOrigin = 0
		viewportAxis.visiblePixelSize = 5
		testAxis(
			viewportAxis,
			#[ 0, 1 ],
			#[ 0.0, 2.0, 5.0 ]
		)
	}
	
	@Test
	def void boundaryToEnd() {
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size:     |---------------------------------------------|
		//                         |0 1 2|3 4 5 6 7 8 9|0 1 2 3 4 5 6 7 8 9 0 1 2|
		viewportAxis.viewportOrigin = 2
		viewportAxis.visiblePixelSize = 23
		testAxis(
			viewportAxis,
			#[ 1, 2, 3 ],
			#[ 0.0, 3.0, 10.0, 23.0 ]
		)
	}
	
	@Test
	def void boundaryToBoundary() {
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size:     |-------------------|
		//                         |0 1 2|3 4 5 6 7 8 9|
		viewportAxis.viewportOrigin = 2
		viewportAxis.visiblePixelSize = 10
		testAxis(
			viewportAxis,
			#[ 1, 2 ],
			#[ 0.0, 3.0, 10.0 ]
		)
	}
	
	@Test
	def void beginningToMidpoint() {
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size: |---------------|
		//                     |0 1|2 3 4|5 6 7 8 9 0 1|
		viewportAxis.viewportOrigin = 0
		viewportAxis.visiblePixelSize = 8
		testAxis(
			viewportAxis,
			#[ 0, 1, 2 ],
			#[ 0.0, 2.0, 5.0, 12.0 ]
		)
	}
	
	@Test
	def void beforeMidpointToEnd() {
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size:                 |---------------------------------|
		//                               |3 2 1 0 1 2 3|4 5 6 7 8 9 0 1 2 3 4 5 6|
		viewportAxis.viewportOrigin = 8
		viewportAxis.visiblePixelSize = 17
		testAxis(
			viewportAxis,
			#[ 2, 3 ],
			#[ -0.0, 4.0, 17.0 ],
			#[ -3.0, 4.0 ],
			#[ 7.0, 13.0 ]
		)
	}
	
	@Test
	def void afterMidpointToEnd() {
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size:                                   |-------------------|
		//                                             |4 5 6 7 8 9 0 1 2 3 4 5 6|
		viewportAxis.viewportOrigin = 17
		viewportAxis.visiblePixelSize = 10
		testAxis(
			viewportAxis,
			#[ 3 ],
			#[ -0.0, 8.0 ],
			#[ -5.0 ],
			#[ 13.0 ]
		)
	}
	
	@Test
	def void midpointToMidpoint() {
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size:         |-----------------------------|
		//                         |2 1 0|1 2 3 4 5 6 7|8 9 0 1 2 3 4 5 6 7 8 9 0|
		viewportAxis.viewportOrigin = 4
		viewportAxis.visiblePixelSize = 15
		testAxis(
			viewportAxis,
			#[ 1, 2, 3 ],
			#[ 0.0, 1.0, 8.0, 21.0 ],
			#[ -2.0, 1.0, 8.0 ],
			#[ 3.0, 7.0, 13.0 ]
		)
	}
	
	@Test
	def void expandRight() {
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size:         |-------------|+++++|
		//                         |2 1 0|1 2 3 4 5 6 7|8 9 0 1 2 3 4 5 6 7 8 9 0|
		viewportAxis.viewportOrigin = 4
		viewportAxis.visiblePixelSize = 7
		testAxis(
			"before expand",
			viewportAxis,
			#[ 1, 2 ],
			#[ -0.0, 1.0, 8.0 ],
			#[ -2.0, 1.0 ],
			#[ 3.0, 7.0 ]
		)
		
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size:         |+++++++++++++++++++|
		//                         |2 1 0|1 2 3 4 5 6 7|8 9 0 1 2 3 4 5 6 7 8 9 0|
		viewportAxis.visiblePixelSize = 10
		viewportAxis.handleResize
		testAxis(
			"after expand",
			viewportAxis,
			#[ 1, 2, 3 ],
			#[ 0.0, 1.0, 8.0, 21.0 ],
			#[ -2.0, 1.0, 8.0 ],
			#[ 3.0, 7.0, 13.0 ]
		)
	}
	
	@Test
	def void expandBeyondEnd() {
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size:         |-------------|+++++++++++++++++++++++++++++++++|
		//                         |2 1 0|1 2 3 4 5 6 7|8 9 0 1 2 3 4 5 6 7 8 9 0|1 2 3
		viewportAxis.viewportOrigin = 4
		viewportAxis.visiblePixelSize = 7
		testAxis(
			"before expand",
			viewportAxis,
			#[ 1, 2 ],
			#[ 0.0, 1.0, 8.0 ],
			#[ -2.0, 1.0 ],
			#[ 3.0, 7.0 ]
		)
		
		//   segment position: | 0 |  1  |      2      |            3            |
		//     pixel location:  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4
		// visible pixel size:   |+++++++++++++++++++++++++++++++++++++++++++++++|
		//                     |1 0|1 2 3|4 5 6 7 8 9 0|1 2 3 4 5 6 7 8 9 0 1 2 3|
		viewportAxis.visiblePixelSize = 24
		viewportAxis.handleResize
		testAxis(
			"after expand",
			viewportAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 1.0, 4.0, 11.0, 24.0 ],
			#[ -1.0, 1.0, 4.0, 11.0 ],
			#[ 2.0, 3.0, 7.0, 13.0 ]
		)
	}
	
}