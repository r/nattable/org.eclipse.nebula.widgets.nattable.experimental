package org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.hideshow

import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.junit.Before
import org.junit.Test

import static org.eclipse.nebula.widgets.nattable.core.layer.AxisTest.*

class HideShowAxisTest {
	
	Axis underlyingAxis
	HideShowAxis hideShowAxis
	
	@Before
	def void before() {
		underlyingAxis = new AxisImpl(4, 100)
		hideShowAxis = new HideShowAxis(underlyingAxis)
	}
	
	@Test
	def void baseline() {
		testAxis(
			"underlyingAxis",
			underlyingAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
		
		testAxis(
			"hideShowAxis",
			hideShowAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void hideShowBeginning() {
		hideShowAxis.hideSegmentId(0L)
		
		testAxis(
			"hide",
			hideShowAxis,
			#[ 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0 ]
		)
		
		hideShowAxis.showSegmentId(0L)
		
		testAxis(
			"show",
			hideShowAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void hideShowMiddle() {
		hideShowAxis.hideSegmentId(1L)
		
		testAxis(
			"hide",
			hideShowAxis,
			#[ 0, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0 ]
		)
		
		hideShowAxis.showSegmentId(1L)
		
		testAxis(
			"show",
			hideShowAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void hideShowEnd() {
		hideShowAxis.hideSegmentId(3L)
		
		testAxis(
			"hide",
			hideShowAxis,
			#[ 0, 1, 2 ],
			#[ 0.0, 100.0, 200.0, 300.0 ]
		)
		
		hideShowAxis.showSegmentId(3L)
		
		testAxis(
			"show",
			hideShowAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
	@Test
	def void hideShowMultiple() {
		hideShowAxis.hideSegmentId(3L)
		
		testAxis(
			"hide 3",
			hideShowAxis,
			#[ 0, 1, 2 ],
			#[ 0.0, 100.0, 200.0, 300.0 ]
		)
		
		hideShowAxis.hideSegmentId(0L)
		
		testAxis(
			"hide 0",
			hideShowAxis,
			#[ 1, 2 ],
			#[ 0.0, 100.0, 200.0 ]
		)
		
		hideShowAxis.hideSegmentId(1L)
		
		testAxis(
			"hide 1",
			hideShowAxis,
			#[ 2 ],
			#[ 0.0, 100.0 ]
		)
		
		hideShowAxis.showSegmentId(1L)
		
		testAxis(
			"show 1",
			hideShowAxis,
			#[ 1, 2 ],
			#[ 0.0, 100.0, 200.0 ]
		)
		
		hideShowAxis.showSegmentId(3L)
		
		testAxis(
			"show 3",
			hideShowAxis,
			#[ 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0 ]
		)
		
		hideShowAxis.showSegmentId(0L)
		
		testAxis(
			"show 0",
			hideShowAxis,
			#[ 0, 1, 2, 3 ],
			#[ 0.0, 100.0, 200.0, 300.0, 400.0 ]
		)
	}
	
}