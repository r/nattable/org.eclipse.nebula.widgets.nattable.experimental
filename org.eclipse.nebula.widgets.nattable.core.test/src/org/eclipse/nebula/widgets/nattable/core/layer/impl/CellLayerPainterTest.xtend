package org.eclipse.nebula.widgets.nattable.core.layer.impl

import org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangle
import org.eclipse.nebula.widgets.nattable.core.graphics.GraphicsContext
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Spy
import org.mockito.runners.MockitoJUnitRunner

import static org.junit.Assert.*
import static org.mockito.Matchers.*

import static extension org.eclipse.nebula.widgets.nattable.core.layer.LayerInvariants.*
import static extension org.mockito.Mockito.*

@RunWith(typeof(MockitoJUnitRunner))
class CellLayerPainterTest {
	
	//   100   100   100   100
	// +-----+-----+-----+-----+
	// | 0,0 | 1,0 | 2,0 | 3,0 | 20
	// +-----+-----+-----+-----+
	// | 0,1 | 1,1 | 2,1 | 3,1 | 20
	// +-----+-----+-----+-----+
	// | 0,2 | 1,2 | 2,2 | 3,2 | 20
	// +-----+-----+-----+-----+
	@Spy DummyLayer testLayer = new DummyLayer(
			new AxisImpl(4, 100),
			new AxisImpl(3, 20)
		)
	CellLayerPainter layerPainter = new CellLayerPainter
	@Mock GraphicsContext testGC
	
	@Test
	def void baseline() {
		assertEquals(4, testLayer.columnCount)
		assertEquals(3, testLayer.rowCount)
	}
	
	@Test
	def void paintEverything() {
		val clipBounds = testLayer.pixelBounds
		when(testGC.clipBounds).thenReturn(clipBounds)
		layerPainter.paintLayer(testLayer, testLayer.pixelArea, testGC)
		verify(testLayer, 12.times).getCell(anyInt, anyInt)
	}
	
	@Test
	def void clipBoundsLargerThanLayer() {
		when(testGC.clipBounds).thenReturn(new PixelRectangle(0, 0, 500, 100))
		layerPainter.paintLayer(testLayer, testLayer.pixelArea, testGC)
		verify(testLayer, 12.times).getCell(anyInt, anyInt)
	}
	
	@Test
	def void paintSome_aligned() {
		when(testGC.clipBounds).thenReturn(new PixelRectangle(100, 20, 200, 20))
		layerPainter.paintLayer(testLayer, testLayer.pixelArea, testGC)
		verify(testLayer, 2.times).getCell(anyInt, anyInt)
	}
	
	@Test
	def void paintSome_unaligned() {
		when(testGC.clipBounds).thenReturn(new PixelRectangle(50, 22, 200, 20))
		layerPainter.paintLayer(testLayer, testLayer.pixelArea, testGC)
		verify(testLayer, 6.times).getCell(anyInt, anyInt)
	}
	
}