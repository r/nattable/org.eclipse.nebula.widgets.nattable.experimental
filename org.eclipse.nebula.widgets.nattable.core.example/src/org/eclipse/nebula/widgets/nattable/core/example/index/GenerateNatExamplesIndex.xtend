package org.eclipse.nebula.widgets.nattable.core.example.index

import java.io.File
import java.io.FileInputStream
import java.io.FileWriter
import java.lang.reflect.Modifier
import java.util.LinkedHashMap
import java.util.LinkedHashSet
import org.eclipse.nebula.widgets.nattable.core.example.NatExample

import static org.eclipse.nebula.widgets.nattable.core.example.index.NatExamplesIndex.*

import static extension org.eclipse.nebula.widgets.nattable.core.example.index.OrderedIndexUtil.*

/**
 * Generates the natExamplesIndex.properties file. For details on the format of this file, see {@link NatExamplesIndex}.
 */
class GenerateNatExamplesIndex {

	def static void main(String[] args) {
		new GenerateNatExamplesIndex().run
	}
	
	//
	
	/**
	 * Generate the NatExamplesIndex.properties file
	 */
	def void run() {
		findExamples(
			new File(new File("src"), BASE_PACKAGE_PATH),
			new File(new File("bin"), BASE_PACKAGE_PATH)
		)
	}
	
	/**
	 * Recursively find all examples in the given directory and below, accumulating information about them in the examples index.
	 */
	def private void findExamples(File srcDir, File binDir) {
		val newIndexProperties = new LinkedHashMap<String, String>
		
		// Look through bin directory
		for (String s : binDir.list) {
			val f = new File(binDir, s)
			if (f.directory) {
				newIndexProperties.put(f.name, "|" + f.name)
				findExamples(new File(srcDir, s), f)  // Recurse; use corresponding src directory
			} else if (f.name.endsWith(".class")) {
				val exampleClass = f.canonicalPath.replaceAll("\\.class$", "").getExampleClass
				if (exampleClass != null)
					newIndexProperties.put(exampleClass.simpleName, exampleClass.canonicalName)
			}
		}
		
		val indexFile = new File(srcDir, INDEX_FILE_NAME)
		val indexProperties =
			if (indexFile.exists)
				new FileInputStream(indexFile).loadOrderedIndex
			else
				new LinkedHashMap
		
		// Remove any entries in the old index that do not have a corresponding entry in the new index
		val keysToRemove = new LinkedHashSet(indexProperties.keySet)
		keysToRemove.removeAll(newIndexProperties.keySet)
		keysToRemove.forEach[
			println('''Removing entry for «it» from the index as it no longer exists.''')
			indexProperties.remove(it)
		]
		
		// Add new entries from the new index into the old index
		val keysToAdd = new LinkedHashSet(newIndexProperties.keySet)
		keysToAdd.removeAll(indexProperties.keySet)
		keysToAdd.forEach[
			println('''Adding new entry for «it» to the index.''')
			indexProperties.put(it, newIndexProperties.get(it))
		]
		
		// Write index properties
		if (!indexProperties.empty) {
			val writer = new FileWriter(indexFile)
			indexProperties.storeOrderedIndex(writer)
			writer.close
		}
	}
	
	/**
	 * @return The example class in the given example path, or null if none exists.
	 */
	def Class<? extends NatExample> getExampleClass(String examplePath) {
		// Find class
		var className = examplePath.replace("/", ".")
		var Class<?> clazz = null
		while (clazz == null && className.indexOf(".") >= 0) {
			try {
				clazz = Class::forName(className)
			} catch (ClassNotFoundException e) {
				// Chop off prefix and try again
				className = className.replaceFirst("^[^.]*\\.", "")
			}
		}
		
		// Check if this is a concrete NatExample
		if (clazz != null && !Modifier::isAbstract(clazz.modifiers) && typeof(NatExample).isAssignableFrom(clazz))
			return clazz as Class<? extends NatExample>
	}
	
}