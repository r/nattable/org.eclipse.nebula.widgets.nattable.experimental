package org.eclipse.nebula.widgets.nattable.core.example.index.node

import org.eclipse.nebula.widgets.nattable.core.example.NatExample

class NatExampleNode extends AbstractIndexNode {
	
	NatExample natExample
	
	new(String path, String displayName, String className) {
		super(path, displayName)
		instantiateNatExample(className)
	}
	
	def private instantiateNatExample(String className) {
		natExample = Class::forName(className).newInstance as NatExample
	}
	
	def NatExample getNatExample() {
		natExample
	}
	
	override getChildNodeNames() { emptyList }
	override getChildNode(String name) {}
	
}