package org.eclipse.nebula.widgets.nattable.core.example.index.node

abstract class AbstractIndexNode implements IndexNode {
	
	val protected String path
	val protected String displayName
	
	new(String path, String displayName) {
		this.path = path
		this.displayName = displayName
	}
	
	override getPath() { path }
	
	override getDisplayName() {
		displayName ?: path.substring(path.lastIndexOf('/') + 1)
	}
	
	override equals(Object that) {
		switch (that) {
			IndexNode: this.path == that.path
			default: false
		}
	}
	
	override hashCode() {
		path.hashCode
	}
	
}