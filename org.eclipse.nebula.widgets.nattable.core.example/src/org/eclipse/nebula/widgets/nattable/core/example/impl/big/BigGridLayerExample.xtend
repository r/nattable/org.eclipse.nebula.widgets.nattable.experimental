package org.eclipse.nebula.widgets.nattable.core.example.impl.big

import java.math.BigDecimal
import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.big.layer.axis.impl.BigAxisImpl
import org.eclipse.nebula.widgets.nattable.core.big.layer.impl.BigDummyLayer
import org.eclipse.nebula.widgets.nattable.core.example.impl.AbstractNatExample
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DimensionallyDependentLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.LayerDataAccessorImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.composite.CompositeLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.ColumnHeaderLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.RowHeaderLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport.ViewportLayer

class BigGridLayerExample extends AbstractNatExample {

	val static MILLION = 1000 * 1000L
	val static BILLION = 1000 * MILLION
	val static TRILLION = 1000 * BILLION
	val static QUADRILLION = 1000 * TRILLION
	
	override getDescription() {
		'''
		A grid with 10 quadrillion columns and 10 quadrillion rows!
		'''
	}
	
	override createLayer() {
		val bodyLayer = new ViewportLayer(new BigDummyLayer(
			// quadrillion, trillion, billion, million, thousand, hundred
			new BigAxisImpl(BigInteger::valueOf(10 * QUADRILLION), BigDecimal::valueOf(200)),  // Horizontal axis
			new BigAxisImpl(BigInteger::valueOf(10 * QUADRILLION), BigDecimal::valueOf(100))  // Vertical axis
		))

		val columnHeaderLayer = new ColumnHeaderLayer(bodyLayer.horizontalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | columnId ]))
		val rowHeaderLayer = new RowHeaderLayer(bodyLayer.verticalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | rowId ]))
		val cornerLayer = new DimensionallyDependentLayer(rowHeaderLayer.horizontalAxis, columnHeaderLayer.verticalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | "" ]))
		
		new CompositeLayer => [
			addRow(cornerLayer, columnHeaderLayer)
			addRow(rowHeaderLayer, bodyLayer)
		]
	}
	
}