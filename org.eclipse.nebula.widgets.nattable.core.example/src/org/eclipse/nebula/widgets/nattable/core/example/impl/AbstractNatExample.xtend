package org.eclipse.nebula.widgets.nattable.core.example.impl

import org.eclipse.nebula.widgets.nattable.core.example.NatExample

abstract class AbstractNatExample implements NatExample {
	
	override getDescription() {}
	
	override start() {}
	override stop() {}
	
}