package org.eclipse.nebula.widgets.nattable.core.example

import org.eclipse.nebula.widgets.nattable.core.layer.Layer

interface NatExample {
	
	def String getDescription()
	
	def Layer createLayer()
	
	def void start()
	def void stop()
	
}