package org.eclipse.nebula.widgets.nattable.core.example.impl

import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.hideshow.HideShowAxis
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.reorder.ReorderAxis
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DimensionallyDependentLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DummyLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.LayerDataAccessorImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.composite.CompositeLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.ColumnHeaderLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.RowHeaderLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport.ViewportLayer

class GridLayerExample extends AbstractNatExample {
	
	override createLayer() {
		val bodyLayer = new ViewportLayer(new DummyLayer(
			// Horizontal axis
			new ReorderAxis(
				new AxisImpl(10, 200) => [ setPixelSizeOfSegmentPosition(100, 0) ]
			) => [ reorderSegmentPosition(0, 2) ],
			
			// Vertical axis
			new HideShowAxis(
				new AxisImpl(10, 100) => [ setPixelSizeOfSegmentPosition(200, 2) ]
			) => [ hideSegmentId(0) ]
		))

		val columnHeaderLayer = new ColumnHeaderLayer(bodyLayer.horizontalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | columnId ]))
		val rowHeaderLayer = new RowHeaderLayer(bodyLayer.verticalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | rowId ]))
		val cornerLayer = new DimensionallyDependentLayer(rowHeaderLayer.horizontalAxis, columnHeaderLayer.verticalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | "" ]))
		
		new CompositeLayer => [
			addRow(cornerLayer, columnHeaderLayer)
			addRow(rowHeaderLayer, bodyLayer)
		]
	}
	
}