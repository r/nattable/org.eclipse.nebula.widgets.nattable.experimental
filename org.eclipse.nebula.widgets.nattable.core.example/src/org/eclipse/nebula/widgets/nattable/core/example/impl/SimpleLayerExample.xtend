package org.eclipse.nebula.widgets.nattable.core.example.impl

import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DummyLayer

class SimpleLayerExample extends AbstractNatExample {
	
	override getDescription() {
		'''
		A single layer with 4 columns and 4 rows
		'''
	}
	
	override createLayer() {
		new DummyLayer(
			new AxisImpl(4, 200),  // Horizontal axis
			new AxisImpl(4, 100)  // Vertical axis
		)
	}
	
}