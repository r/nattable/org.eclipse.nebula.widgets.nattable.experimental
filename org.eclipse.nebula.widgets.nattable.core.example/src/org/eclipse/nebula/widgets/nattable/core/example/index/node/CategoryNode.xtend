package org.eclipse.nebula.widgets.nattable.core.example.index.node

import java.util.LinkedHashMap
import java.util.List

import static org.eclipse.nebula.widgets.nattable.core.example.index.NatExamplesIndex.*

import static extension org.eclipse.nebula.widgets.nattable.core.example.index.OrderedIndexUtil.*

class CategoryNode extends AbstractIndexNode {
	
	val LinkedHashMap<String, String> indexProperties = loadIndexProperties
	
	new(String path, String displayName) {
		super(path, displayName)
	}
	
	def loadIndexProperties() {
		val inputStream = class.getResourceAsStream(path + '/' + INDEX_FILE_NAME)
		if (inputStream == null)
			throw new IllegalStateException('''«path»/«INDEX_FILE_NAME» not found! Please run GenerateNatExamplesIndex from the org.eclipse.nebula.widgets.nattable.core.example project.''')
		
		inputStream.loadOrderedIndex
	}
	
	override getChildNodeNames() {
		indexProperties.keySet.toArray as List<String>
	}
	
	override getChildNode(String name) {
		val propertyValue = indexProperties.get(name)
		if (propertyValue != null)
			createIndexNode(name, propertyValue)
	}
	
	/**
	 * Creates a concrete IndexNode from the given property name and value. The property value is a '|'-delimited string which can represent
	 * a NatExample, or a category. If the property value represents a category, it takes the following form:
	 * <ul>
	 *   <li>|Display name</li>
	 * </ul>
	 * Note the leading '|' character.
	 * <p>
	 * If the property value represents a NatExample, it can take either of the following forms:
	 * <ul>
	 *   <li>fully.qualified.example.class.name</li>
	 *   <li>fully.qualified.example.class.name|Display name</li>
	 * </ul>
	 * 
	 * @param propertyName The name of the property.
	 * @param propertyValue The value of the property.
	 */
	def createIndexNode(String propertyName, String propertyValue) {
		val nodePath = path + '/' + propertyName
		
		val separatorIndex = propertyValue.indexOf('|')
		if (separatorIndex == 0) {
			// Category node
			val nodeDisplayName = propertyValue.substring(1)
			new CategoryNode(nodePath, nodeDisplayName)
		} else {
			// Example node
			val nodeDisplayName = if (separatorIndex > 0) propertyValue.substring(separatorIndex + 1)
			val exampleClassName = if (separatorIndex > 0) propertyValue.substring(0, separatorIndex) else propertyValue
			new NatExampleNode(nodePath, nodeDisplayName, exampleClassName)
		}
	}
	
}