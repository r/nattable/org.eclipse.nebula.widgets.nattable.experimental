package org.eclipse.nebula.widgets.nattable.core.example.impl

import org.eclipse.nebula.widgets.nattable.core.example.impl.AbstractNatExample
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DimensionallyDependentLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DummyLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.LayerDataAccessorImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.composite.CompositeLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.ColumnHeaderLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.RowHeaderLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport.ViewportLayer

class SmallGridLayerExample extends AbstractNatExample {
	
	override getDescription() {
		'''
		A little grid with 4 columns and 4 rows
		'''
	}
	
	override createLayer() {
		val bodyLayer = new ViewportLayer(new DummyLayer(
			new AxisImpl(4, 200),  // Horizontal axis
			new AxisImpl(4, 100)  // Vertical axis
		))

		val columnHeaderLayer = new ColumnHeaderLayer(bodyLayer.horizontalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | columnId ]))
		val rowHeaderLayer = new RowHeaderLayer(bodyLayer.verticalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | rowId ]))
		val cornerLayer = new DimensionallyDependentLayer(rowHeaderLayer.horizontalAxis, columnHeaderLayer.verticalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | "" ]))
		
		new CompositeLayer => [
			addRow(cornerLayer, columnHeaderLayer)
			addRow(rowHeaderLayer, bodyLayer)
		]
	}
	
}