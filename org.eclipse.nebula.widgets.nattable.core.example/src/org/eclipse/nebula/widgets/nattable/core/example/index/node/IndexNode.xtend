package org.eclipse.nebula.widgets.nattable.core.example.index.node

import java.util.List

interface IndexNode {
	
	def String getPath()
	def String getDisplayName()
	
	def List<String> getChildNodeNames()
	def IndexNode getChildNode(String name)
	
}