package org.eclipse.nebula.widgets.nattable.core.example.impl

import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DimensionallyDependentLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.DummyLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.LayerDataAccessorImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.composite.CompositeLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.ColumnHeaderLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.header.RowHeaderLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport.ViewportLayer

class MultiViewportExample extends AbstractNatExample {
	
	override createLayer() {
		val bodyLayer = new CompositeLayer => [
			val viewportOne = new ViewportLayer => [
					underlyingLayer = new DummyLayer(
						new AxisImpl(4, 150),  // Horizontal axis
						new AxisImpl(10, 100)  // Vertical axis
					)
					horizontalAxis.minPixelLocation = 0
					horizontalAxis.maxPixelSize = 150
					verticalScrollBarVisible = false
				]
			val viewportTwo = new ViewportLayer => [
					underlyingLayer = new DummyLayer(
						new AxisImpl(10, 150),  // Horizontal axis
						new AxisImpl(10, 100)  // Vertical axis
					)
					verticalAxis = viewportOne.verticalAxis
				]
			
			addRow(
				viewportOne,
				viewportTwo
			)
		]

		val columnHeaderLayer = new ColumnHeaderLayer(bodyLayer.horizontalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | columnId ]))
		val rowHeaderLayer = new RowHeaderLayer(bodyLayer.verticalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | rowId ]))
		val cornerLayer = new DimensionallyDependentLayer(rowHeaderLayer.horizontalAxis, columnHeaderLayer.verticalAxis, new LayerDataAccessorImpl([ layer, columnId, rowId | "" ]))
		
		new CompositeLayer => [
			addRow(cornerLayer, columnHeaderLayer)
			addRow(rowHeaderLayer, bodyLayer)
		]
	}
	
}