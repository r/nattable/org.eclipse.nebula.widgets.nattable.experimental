package org.eclipse.nebula.widgets.nattable.core.example.index

import java.io.InputStream
import java.io.Writer
import java.util.LinkedHashMap
import java.io.BufferedReader
import java.io.InputStreamReader

class OrderedIndexUtil {
	
	/**
	 * Reads a properties file into a LinkedHashMap, preserving the order of entries.
	 */
	def static LinkedHashMap<String, String> loadOrderedIndex(InputStream inputStream) {
		val index = new LinkedHashMap<String, String>
		
		val reader = new BufferedReader(new InputStreamReader(inputStream))
		var line = reader.readLine
		while (line != null) {
			val i = line.indexOf('=')
			val key = line.substring(0, i)
			val value = line.substring(i + 1)
			
			index.put(key, value)
			
			line = reader.readLine
		}
		reader.close
		
		index
	}
	
	/**
	 * Saves a LinkedHashMap into a properties file, preserving the order of entries.
	 */
	def static void storeOrderedIndex(LinkedHashMap<String, String> index, Writer writer) {
		for (key : index.keySet)
			writer.write(key + "=" + index.get(key) + "\n")
	}
	
}