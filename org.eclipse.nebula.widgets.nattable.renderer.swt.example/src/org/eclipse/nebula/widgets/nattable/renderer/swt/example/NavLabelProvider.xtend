package org.eclipse.nebula.widgets.nattable.renderer.swt.example

import org.eclipse.jface.viewers.LabelProvider
import org.eclipse.nebula.widgets.nattable.core.example.index.NatExamplesIndex

class NavLabelProvider extends LabelProvider {
	
	val NavContentProvider contentProvider

	new(NavContentProvider contentProvider) {
		this.contentProvider = contentProvider
	}
	
	override getText(Object element) {
		val nodePath = element as String
		NatExamplesIndex::getNode(nodePath).displayName
	}
	
}