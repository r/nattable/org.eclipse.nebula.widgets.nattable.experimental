package org.eclipse.nebula.widgets.nattable.renderer.swt.example

import org.eclipse.jface.viewers.ITreeContentProvider
import org.eclipse.jface.viewers.Viewer
import org.eclipse.nebula.widgets.nattable.core.example.index.NatExamplesIndex
import org.eclipse.nebula.widgets.nattable.core.example.index.node.IndexNode

class NavContentProvider implements ITreeContentProvider {

	override dispose() {}

	override inputChanged(Viewer viewer, Object oldInput, Object newInput) {}

	override getParent(Object element) {
		val nodePath = element as String
		val lastSlashIndex = nodePath.lastIndexOf('/')
		if (lastSlashIndex < 0)
			return null
		else
			return nodePath.substring(0, lastSlashIndex)
	}

	override hasChildren(Object element) {
		val nodePath = element as String
		val node = NatExamplesIndex::getNode(nodePath)
		!node.childNodeNames.empty
	}

	override getChildren(Object parent) {
		val nodePath = parent as String
		val node = NatExamplesIndex::getNode(nodePath)
		node.childNodeNames.map[ nodePath + '/' + it ]
	}

	override getElements(Object inputElement) {
		val node = inputElement as IndexNode
		node.childNodeNames
	}

}
