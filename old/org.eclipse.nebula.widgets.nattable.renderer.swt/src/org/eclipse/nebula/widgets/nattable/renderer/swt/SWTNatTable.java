package org.eclipse.nebula.widgets.nattable.renderer.swt;

import org.eclipse.nebula.widgets.nattable.core.NatTable;
import org.eclipse.nebula.widgets.nattable.core.INatTableConfiguration;
import org.eclipse.nebula.widgets.nattable.core.layer.ILayerListener;
import org.eclipse.nebula.widgets.nattable.core.layer.event.ILayerEvent;
import org.eclipse.nebula.widgets.nattable.core.painter.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ScrollBar;

/**
 * The SWT rendering implementation of the NatTable. Basically a {@link Canvas} on which
 * the table will be painted. 
 * 
 * TODO some more documentation
 * 
 * @author Dirk Fauth
 *
 */
public class SWTNatTable extends Canvas implements PaintListener, ILayerListener {

	/**
	 * The presentation model that contains the logic.
	 */
	private NatTable model;
	
	/**
	 * The default style options for rendering the SWT NatTable composite.
	 * @see SWT#NO_BACKGROUND
	 * @see SWT#NO_REDRAW_RESIZE
	 * @see SWT#DOUBLE_BUFFERED
	 * @see SWT#V_SCROLL
	 * @see SWT#H_SCROLL
	 */
	public static final int DEFAULT_STYLE_OPTIONS = SWT.NO_BACKGROUND | SWT.NO_REDRAW_RESIZE | SWT.DOUBLE_BUFFERED  | SWT.V_SCROLL | SWT.H_SCROLL;

	/**
	 * Creates a new NatTable presentation instance for SWT rendering, using the specified configuration and
	 * the defined default style options.
	 * @param parent A composite control which will be the parent of the new instance (cannot be null).
	 * @param configuration The configuration that will be used to build up the new NatTable instance.
	 */
	public SWTNatTable(Composite parent, INatTableConfiguration configuration) {
		this(parent, configuration, DEFAULT_STYLE_OPTIONS);
	}

	/**
	 * Creates a new NatTable presentation instance for SWT rendering for the specified NatTable model, 
	 * using the defined default style options.
	 * @param parent A composite control which will be the parent of the new instance (cannot be null).
	 * @param mNatTable The NatTable model instance to build this presentation for.
	 */
	public SWTNatTable(Composite parent, NatTable mNatTable) {
		this(parent, mNatTable, DEFAULT_STYLE_OPTIONS);
	}

	/**
	 * Creates a new NatTable presentation instance for SWT rendering, using the specified configuration 
	 * and style options.
	 * @param parent A composite control which will be the parent of the new instance (cannot be null).
	 * @param configuration The configuration that will be used to build up the NatTable instance.
	 * @param style The style of control to construct.
	 */
	public SWTNatTable(Composite parent, INatTableConfiguration configuration, int style) {
		this(parent, new NatTable(configuration), style);
	}

	/**
	 * Creates a new NatTable presentation instance for SWT rendering for the specified NatTable model and 
	 * style options.
	 * @param parent A composite control which will be the parent of the new instance (cannot be null).
	 * @param mNatTable The NatTable model instance to build this presentation for.
	 * @param style The style of control to construct.
	 */
	public SWTNatTable(Composite parent, NatTable mNatTable, int style) {
		super(parent, style);

		//create a new model based on the given configuration
		setModel(mNatTable);
		
		// Disable scroll bars by default; if a Viewport is available, it will enable the scroll bars
		disableScrollBar(getHorizontalBar());
		disableScrollBar(getVerticalBar());

	}

	/**
	 * Sets the {@link SWTNatTable} presentation model instance to this SWT NatTable renderer.
	 * This way it is possible to change the presentation model at runtime by keeping the 
	 * renderer within the UI.
	 * @param model The presentation model to set.
	 */
	public void setModel(NatTable model) {
		if (this.model != null) {
			//if there is already a model set and we are setting an new model
			//we have to remove ourself as a listener
			this.model.removeLayerListener(this);
		}
		this.model = model;
		//register myself as listener
		this.model.addLayerListener(this);
	}

	@Override
	public void handleLayerEvent(ILayerEvent event) {
		// TODO Auto-generated method stub
//	    if (event instanceof IVisualChangeEvent) {
//    	conflaterChain.addEvent(event);
//    }
//
//	if (event instanceof CellSelectionEvent) {
//		Event e = new Event();
//		e.widget = this;
//		try {
//			notifyListeners(SWT.Selection, e);
//		} catch (RuntimeException re) {
//			re.printStackTrace();
//		}
//	}
	}
	
	@Override
	public void paintControl(final PaintEvent event) {
		model.paintNatTable(
				new SWTGraphicsContext(event.gc),
				new Rectangle(event.x, event.y, event.width, event.height));
	}

	
	//TODO should this be something that is called by the presentation model, firing an event that is catched here?
	public void updateResize() {
		updateResize(true);
	}

	/**
	 * Update the table screen by re-calculating everything again. It should not
	 * be called too frequently.
	 *
	 * @param redraw
	 *            true to redraw the table
	 */
	private void updateResize(final boolean redraw) {
		if (isDisposed()) {
			return;
		}
//		doCommand(new RecalculateScrollBarsCommand());
		if (redraw) {
			redraw();
		}
	}

	private void disableScrollBar(ScrollBar scrollBar) {
		scrollBar.setMinimum(0);
		scrollBar.setMaximum(1);
		scrollBar.setThumb(1);
		scrollBar.setEnabled(false);
	}

}
