package org.eclipse.nebula.widgets.nattable.renderer.swt;

import org.eclipse.nebula.widgets.nattable.core.painter.IGraphicsContext;
import org.eclipse.nebula.widgets.nattable.core.painter.Point;
import org.eclipse.nebula.widgets.nattable.core.style.IStyle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;

/**
 * Proxy implementation of {@link IGraphicsContext} to provide the drawing
 * capabilities by {@link GC} so the NatTable can be painted using this.
 * 
 * @author Dirk Fauth
 *
 */
public class SWTGraphicsContext implements IGraphicsContext {

	/**
	 * The SWT {@link GC} which is proxied by this implementation.
	 */
	private final GC gc;
	
	/**
	 * Create a new proxy instance for the specified {@link GC}.
	 * @param gc The SWT {@link GC} which is proxied by this implementation.
	 */
	public SWTGraphicsContext(final GC gc) {
		if (gc == null) {
			throw new IllegalArgumentException("gc can not be null"); //$NON-NLS-1$
		}
		this.gc = gc;
	}
	
	@Override
	public void initStyle(IStyle style) {
		//TODO implement like AbstactTextPainter setupGCFromConfig()
		//first store the current values within the gc so they can be restored afterwards
		//then set the values extracted out of the style
		//also underline, strikethrough and paintBG should be considered and stored in here
		//as they are not part of text styling in the GC
	}
	
	@Override
	public void resetStyle() {
		//TODO implement reset
	}
	
	@Override
	public void drawText(String text, int x, int y) {
		this.gc.drawText(text, x, y, SWT.DRAW_TRANSPARENT | SWT.DRAW_DELIMITER);
		
		//TODO draw underline and strikethrough in here instead of the TextPainter itself
	}

	@Override
	public void drawVerticalText(String text, int x, int y, boolean up) {
		GraphicsUtils.drawVerticalText(text, x, y, gc, up ? SWT.UP : SWT.DOWN);
		
		//TODO draw underline and strikethrough in here, or let it do the GraphicsUtils
	}
	
	@Override
	public void drawLine(int x1, int y1, int x2, int y2) {
		this.gc.drawLine(x1, y1, x2, y2);
	}

	@Override
	public Point calculateTextExtend(String text) {
		org.eclipse.swt.graphics.Point swtPoint = this.gc.textExtent(text);
		return new Point(swtPoint.x, swtPoint.y);
	}
}
