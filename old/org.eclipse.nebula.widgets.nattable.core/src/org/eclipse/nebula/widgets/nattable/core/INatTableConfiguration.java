package org.eclipse.nebula.widgets.nattable.core;

/**
 * The NatTableConfiguration is responsible for configuring a NatTable instance.
 * This means to compose the layer stacks and configure the layers and NatTable itself.
 * In the context of dependency injection, it is also responsible for adding created
 * instances to the dependency injection context.
 * 
 * @author Dirk Fauth
 *
 */
public interface INatTableConfiguration {

	/**
	 * Starts constructing the NatTable with the given configuration.
	 */
	public void startConstruction();
	
	/**
	 * Sets a value to be associated with a given name in this configuration. 
	 * @param name the name to store a value for
	 * @param value The value to be stored
	 */
	void set(String name, Object value);
	
	/**
	 * Sets a value to be associated with a given class in this configuration. 
	 * @param clazz The class to store a value for
	 * @param value The value to be stored
	 * @see #set(String, Object)
	 */
	<T> void set(Class<T> clazz, T value);

	/**
	 * Creates an instance of the given class.
	 * @param clazz The class for which an instance should be created
	 * @return The created instance for the given class.
	 */
	<T> T create(Class<T> clazz);
}
