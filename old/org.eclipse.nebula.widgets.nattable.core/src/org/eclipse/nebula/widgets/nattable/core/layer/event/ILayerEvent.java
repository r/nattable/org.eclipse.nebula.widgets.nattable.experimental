/*******************************************************************************
 * Copyright (c) 2012 Original authors and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Original authors and others - initial API and implementation
 ******************************************************************************/
package org.eclipse.nebula.widgets.nattable.core.layer.event;

import org.eclipse.nebula.widgets.nattable.core.layer.ILayer;


/**
 * Event fired by {@link ILayerCommandHandler} implementations (usually to signal the handling of a {@link ILayerCommand}).<br/>
 * Every layer in the grid is given a chance to respond to an event via {@link ILayer#handleLayerEvent(ILayerEvent)},
 * as {@link ILayerEvent}s are fired the layer stack upwards without consuming the event.
 * 
 * @see ILayerEventHandler
 */
public interface ILayerEvent {

	/**
	 * Convert the column/row positions carried by the event to the layer about to
	 * handle the event.
	 * @param localLayer layer about to receive the event
	 * @return <code>true</code> if successfully converted, <code>false</code> otherwise
	 */
	boolean convertToLocal(ILayer localLayer);

	/**
	 * @return A cloned copy of the event. This cloned copy is provided to each listener.
	 */
	ILayerEvent cloneEvent();
	
}
