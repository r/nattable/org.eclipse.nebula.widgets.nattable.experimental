/*******************************************************************************
 * Copyright (c) 2012 Original authors and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Original authors and others - initial API and implementation
 ******************************************************************************/
package org.eclipse.nebula.widgets.nattable.core.style;

/**
 * Used to store attributes reflecting a (usually display) style.
 */
public interface IStyle {
	
	/**
	 * Returns the value for the specified {@link ConfigAttribute} configured for
	 * this style or <code>null</code> if there is no value configured.
	 * @param styleAttribute The {@link ConfigAttribute} whose value is requested.
	 * @return The value for the specified {@link ConfigAttribute} or <code>null</code>
	 * 			if this style doesn't contain a value for it.
	 */
	public <T> T getAttributeValue(ConfigAttribute<T> styleAttribute);
	
	/**
	 * Set the specified {@link ConfigAttribute} as a value of this style.
	 * @param styleAttribute The {@link ConfigAttribute} to set.
	 * @param value The value for the specified {@link ConfigAttribute}.
	 */
	public <T> void setAttributeValue(ConfigAttribute<T> styleAttribute, T value);
}
