/*******************************************************************************
 * Copyright (c) 2012 Original authors and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Original authors and others - initial API and implementation
 ******************************************************************************/
package org.eclipse.nebula.widgets.nattable.core.layer.event;

/**
 * Event handler for {@link ILayerEvent}s.
 * 
 * @param <T> The type of the {@link ILayerEvent} objects this handler can handle.
 */
public interface ILayerEventHandler <T extends ILayerEvent> {

	/**
	 * Handles the specified {@link ILayerEvent}.
	 * @param event The event to handle.
	 */
	public void handleLayerEvent(T event);
	
	/**
	 * @return The type of the {@link ILayerEvent} objects this handler can handle.
	 */
	public Class<T> getLayerEventClass();
	
}
