/*******************************************************************************
 * Copyright (c) 2012 Original authors and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Original authors and others - initial API and implementation
 ******************************************************************************/
package org.eclipse.nebula.widgets.nattable.core.painter.cell;

import org.eclipse.nebula.widgets.nattable.core.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.core.layer.cell.ILayerCell;
import org.eclipse.nebula.widgets.nattable.core.painter.IGraphicsContext;
import org.eclipse.nebula.widgets.nattable.core.painter.Rectangle;

/**
 * Implementations are responsible for painting a cell.<br/>
 * 
 * Custom {@link ICellPainter} can be registered in the {@link IConfigRegistry}.
 * This is a mechanism for plugging in custom cell painting.
 * 
 * TODO remove the IConfigRegistry from the API and modify the code for usage of DI
 */
public interface ICellPainter {
	
	/**
	 * Paint the specified cell to the NatTable.
	 * @param cell The {@link ILayerCell} to paint.
	 * @param gc The {@link IGraphicsContext} that should be used for painting
	 * @param bounds The bounds of the cell.
	 * @param configRegistry The configuration that should be used for retrieving the
	 * 			style information
	 */
	public void paintCell(ILayerCell cell, IGraphicsContext gc, Rectangle bounds, IConfigRegistry configRegistry);

	/**
	 * Get the preferred width of the cell when rendered by this painter. Used for auto-resize.
	 * @param cell The {@link ILayerCell} whose preferred width is requested.
	 * @param gc The {@link IGraphicsContext} that is used for painting
	 * @param configRegistry The configuration that should be used for retrieving the
	 * 			style information
	 * @return The preferred width of the cell when rendered by this painter.
	 */
	public int getPreferredWidth(ILayerCell cell, IGraphicsContext gc, IConfigRegistry configRegistry);

	/**
	 * Get the preferred height of the cell when rendered by this painter. Used for auto-resize.
	 * @param cell The {@link ILayerCell} whose preferred width is requested.
	 * @param gc The {@link IGraphicsContext} that is used for painting
	 * @param configRegistry The configuration that should be used for retrieving the
	 * 			style information
	 * @return The preferred height of the cell when rendered by this painter.
	 */
	public int getPreferredHeight(ILayerCell cell, IGraphicsContext gc, IConfigRegistry configRegistry);
	
	
	/**
	 * TODO javadoc
	 * 
	 * @param x
	 * @param y
	 * @param cell
	 * @param gc
	 * @param adjustedCellBounds
	 * @param configRegistry
	 * @return
	 */
	public ICellPainter getCellPainterAt(int x, int y, ILayerCell cell, IGraphicsContext gc, Rectangle adjustedCellBounds, IConfigRegistry configRegistry);
	
}
