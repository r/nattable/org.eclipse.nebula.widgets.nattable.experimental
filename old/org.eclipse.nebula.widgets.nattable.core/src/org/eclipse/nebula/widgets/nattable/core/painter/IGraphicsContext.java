package org.eclipse.nebula.widgets.nattable.core.painter;

import org.eclipse.nebula.widgets.nattable.core.style.IStyle;

/**
 * Interface for implementing a proxy to a UI toolkit dependent graphics context
 * implementation. The graphics context is at the end the on who paints lines
 * shapes etc. to the canvas to create the NatTable.
 * 
 * e.g. org.eclipse.swt.graphics.GC
 * 
 * @author Dirk Fauth
 *
 */
public interface IGraphicsContext {

	/**
	 * 
	 * @param style {@link IStyle} instance containing the style information that
	 * 			should be used in the following painting operations.
	 */
	void initStyle(IStyle style);
	
	/**
	 * Resets the styling that was set by calling initStyle(IStyle).
	 * Usually this don't need to called, as every drawing operation in a NatTable
	 * painter will set the style information previous to a drawing operation,
	 * but to have a clean API, this method should be provided.
	 * 
	 * <p>You could think of a use case where a custom control needs to be painted
	 * into a NatTable. If this custom control uses the same graphics context then
	 * the NatTable, it might cause strange effects if the style is not resetted
	 * to the previous values. 
	 */
	void resetStyle();
	
	/**
	 * Draws the specified text at the position specified by x and y coordinates.
	 * Ensure that drawing the text doesn't draw a background and line delimiters 
	 * are handled correctly.
	 * @param text The text to draw.
	 * @param x the x coordinate of the top left corner of the rectangular area where the text is to be drawn
	 * @param y the y coordinate of the top left corner of the rectangular area where the text is to be drawn
	 */
	void drawText(String text, int x, int y);
	
	  /**
	   * Draws the specified text vertically at the position specified by x and y coordinates.
	   * The vertical translation is done by rotating plus or minus 90 degrees dependent on the
	   * specified up flag. Ensure that drawing the text doesn't draw a background and line 
	   * delimiters are handled correctly.
	   *
	   * @param text the text to draw
	   * @param x the x coordinate of the top left corner of the drawing rectangle
	   * @param y the y coordinate of the top left corner of the drawing rectangle
	   * @param up if <code>true</code> the text will be drawed from the bottom to the top
	   * 			(this means the text will be rotated minus 90 degrees), while <code>false</code>
	   * 			will draw the text from top to bottom (which means rotation plus 90 degrees).
	   */
	void drawVerticalText(String text, int x, int y, boolean up);
	
	/**
	 * Draws a line, using the foreground color, between the points 
	 * (<code>x1</code>, <code>y1</code>) and (<code>x2</code>, <code>y2</code>).
	 *
	 * @param x1 the first point's x coordinate
	 * @param y1 the first point's y coordinate
	 * @param x2 the second point's x coordinate
	 * @param y2 the second point's y coordinate
	 */
	void drawLine(int x1, int y1, int x2, int y2);
	
	/**
	 * Returns the extent of the given string. Tab expansion and
	 * carriage return processing are performed.
	 * <p>
	 * The <em>extent</em> of a string is the width and height of
	 * the rectangular area it would cover if drawn in a particular
	 * font (in this case, the current font in the receiver).
	 * </p>
	 *
	 * @param text the text to measure
	 * @return a point containing the extent of the string
	 */
	Point calculateTextExtend(String text);
}
