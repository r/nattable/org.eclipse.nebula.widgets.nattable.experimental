/*******************************************************************************
 * Copyright (c) 2012 Original authors and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Original authors and others - initial API and implementation
 ******************************************************************************/
package org.eclipse.nebula.widgets.nattable.core.layer;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.nebula.widgets.nattable.core.layer.cell.IConfigLabelAccumulator;

/**
 * The LabelStack is a list of labels attached to a ILayerCell.
 * With the label stack mechanism it is possible to configure
 * special rendering or behavior, dependent on labels included in
 * the stack.
 * 
 * <p>A LabelStack can not contain duplicate labels. So you might
 * want to think of the LabelStack as an ordered java.util.Set
 *
 * <p>To add labels to the LabelStack, usually a {@link IConfigLabelAccumulator}
 * is used.
 */
public class LabelStack {

	/** 
	 * List implementation saves the overhead of popping labels off
	 * in the {@link #getLabels()} method
	 */
	private final List<String> labels = new LinkedList<String>();
	
	/**
	 * Create a new LabelStack which is initialized with the specified
	 * collection of labels. Duplicate labels are not allowed and will
	 * be filtered on initialization automatically.
	 * @param labels The labels which should be used to initialize the
	 * 			new LabelStack
	 */
	public LabelStack(String...labels) {
		for (String label : labels) {
			if (label != null) {
				addLabel(label);
			}
		}
	}
	
	/**
	 * Adds a label at the bottom of this LabelStack.
	 * The label is only added if it is not present yet.
	 * @param label The label to add to this LabelStack.
	 */
	public void addLabel(String label) {
		if(!hasLabel(label)) {
			labels.add(label);
		}
	}
	
	/**
	 * Adds a label to the top of this LabelStack.
	 * If the specified label is already included in this LabelStack,
	 * it is moved to the top.
	 * @param label The label to add to this LabelStack
	 */
	public void addLabelOnTop(String label) {
		if (hasLabel(label)) {
			removeLabel(label);
		}
		labels.add(0, label);
	}
	
	/**
	 * 
	 * @return The list of labels included in this LabelStack
	 */
	public List<String> getLabels() {
		return labels;
	}
	
	/**
	 * Checks if this LabelStack contains the specified label.
	 * @param label The label whose presence need to be tested.
	 * @return <code>true</code> if this LabelStack contains the specified label.
	 */
	public boolean hasLabel(String label) {
		return labels.contains(label);
	}
	
	/**
	 * Removes the specified label from this LabelStack.
	 * @param label The label to remove.
	 * @return <code>true</code> if this LabelStack contained the specified label.
	 */
	public boolean removeLabel(String label) {
		return labels.remove(label);
	}
	
	@Override
	public String toString() {
		return labels.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		
		if (!(obj instanceof LabelStack))
			return false;
		
		return this.labels.equals(((LabelStack) obj).labels);
	}
	
	@Override
	public int hashCode() {
		return labels.hashCode();
	}
	
}
