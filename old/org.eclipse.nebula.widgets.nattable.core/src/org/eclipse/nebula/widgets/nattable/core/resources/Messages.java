/*******************************************************************************
 * Copyright (c) 2012 Original authors and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Original authors and others - initial API and implementation
 ******************************************************************************/
package org.eclipse.nebula.widgets.nattable.core.resources;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Helper class for localization within the NatTable.
 * Uses String based localization by reading directly out of a {@link ResourceBundle} that
 * is contained in the package structure. A constant based approach like it can be seen in
 * an Eclipse environment is not used here because of the fact that the NatTable might be
 * used in a non OSGi environment.
 */
public class Messages {
	
	/**
	 * The name of the NatTable internal {@link ResourceBundle}
	 */
	private static final String BUNDLE_NAME = "org.eclipse.nebula.widgets.nattable.core.resources.messages"; //$NON-NLS-1$
	
	/**
	 * The NatTable internal {@link ResourceBundle}
	 */
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	/**
	 * Gets a translation for the given key from the NatTable internal resource bundle.
	 * @param key the key for the desired translation
	 * @return the translation for the given key or the key prefixed and suffixed with "!" if 
	 * 			there is no translation found for the given key.
     * @throws NullPointerException if <code>key</code> is <code>null</code>
	 */
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	/**
	 * Gets a translation for the given key from the NatTable internal resource bundle.
	 * Will format the returned value by replacing placeholders with the values specified
	 * in the args parameter.
	 * @param key the key for the desired translation
	 * @param args the arguments which should be used for replacement of placeholders in the 
	 * 			translation
	 * @return the translation for the given key or the key prefixed and suffixed with "!" if 
	 * 			there is no translation found for the given key.
     * @throws NullPointerException if <code>key</code> is <code>null</code>
     * @see Messages#getString(String)
     * @see MessageFormat#format(String, Object...)
	 */
	public static String getString (String key, Object... args) { 
		return MessageFormat.format(getString(key), args); 
	}
	
	private Messages() {}
}
