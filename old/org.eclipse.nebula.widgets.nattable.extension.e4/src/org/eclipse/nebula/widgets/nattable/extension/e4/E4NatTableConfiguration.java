package org.eclipse.nebula.widgets.nattable.extension.e4;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.nebula.widgets.nattable.core.INatTableConfiguration;

/**
 * NatTableConfiguration that supports dependency injection with the Eclipse 4
 * Application Platform. Creates a new IEclipseContext and all configured
 * objects for the NatTable instance are then created and managed by this
 * context.
 * 
 * @author Dirk Fauth
 *
 */
public class E4NatTableConfiguration implements INatTableConfiguration {

	/**
	 * The NatTable local IEclipseContext used for injection of NatTable specific
	 * objects.
	 */
	private IEclipseContext localContext;
	
	public E4NatTableConfiguration(IEclipseContext parentContext) {
		this.localContext = parentContext.createChild();
	}
	
	
	public void startConstruction() {
		/*
		 * TODO this is where the magic should happen
		 * 
		 * we could read a config file or some EMF stuff to start creation
		 * of layers, composing the layer stacks, adding configurations
		 * and whatever we want to to automatically.
		 * 
		 * The set() and make() methods encapsulate the DI stuff in first place
		 * so this method here could also be some kind of replaceable so you
		 * have different kinds of configuration (as just mentioned, file based
		 * or EMF, or whatever)
		 */
	}
	
	@Override
	public void set(String name, Object value) {
		//start the injection to the given value
		ContextInjectionFactory.inject(value, this.localContext);
		//set the value to the local context
		this.localContext.set(name, value);
	}

	@Override
	public <T> void set(Class<T> clazz, T value) {
		//start the injection to the given value
		ContextInjectionFactory.inject(value, this.localContext);
		//set the value to the local context
		this.localContext.set(clazz, value);
	}

	@Override
	public <T> T create(Class<T> clazz) {
		//Let the ContextInjectionFactory create the instance
		T instance = ContextInjectionFactory.make(clazz, this.localContext);
		//as make ContextInjectionFactory.make() doesn't put the created instance
		//into the context itself, we need to do this ourself
		this.localContext.set(clazz, instance);
		return instance;
	}
}
