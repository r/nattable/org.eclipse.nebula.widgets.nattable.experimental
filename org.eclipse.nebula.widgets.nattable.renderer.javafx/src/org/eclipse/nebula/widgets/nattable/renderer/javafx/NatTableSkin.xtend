package org.eclipse.nebula.widgets.nattable.renderer.javafx

import com.sun.javafx.scene.control.skin.SkinBase
import javafx.scene.canvas.Canvas
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.renderer.javafx.graphics.JavaFXGraphicsContext

class NatTableSkin extends SkinBase<JavaFXNatTable, NatTableBehavior> {
	
	val Canvas canvas
	
	new(JavaFXNatTable control) {
		super(control, new NatTableBehavior(control))
		
		canvas = new Canvas
		children += canvas
		
		canvas.widthProperty.bind(widthProperty)
		canvas.heightProperty.bind(heightProperty)
	}
	
	override protected layoutChildren() {
		canvas.paintLayer
	}
	
	def private paintLayer(Canvas canvas) {
		val paintArea = new PixelArea(boundsInLocal.width, boundsInLocal.height)
		val gc = new JavaFXGraphicsContext(canvas)
		skinnable.layerPainter?.paintLayer(skinnable.layer, paintArea, gc)
	}
	
}