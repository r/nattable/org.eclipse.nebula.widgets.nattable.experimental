package org.eclipse.nebula.widgets.nattable.renderer.javafx.graphics

import com.sun.javafx.tk.Toolkit
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangle
import org.eclipse.nebula.widgets.nattable.core.graphics.Color
import org.eclipse.nebula.widgets.nattable.core.graphics.GraphicsContext
import javafx.scene.canvas.Canvas

class JavaFXGraphicsContext implements GraphicsContext {
	
	val Canvas canvas
	val javafx.scene.canvas.GraphicsContext gc
	
	PixelRectangle clipBounds
	
	new(Canvas canvas) {
		this.canvas = canvas
		this.gc = canvas.graphicsContext2D
	}
	
	def Canvas getCanvas() { canvas }
	
	// GraphicsContext interface
	
	override pushState() {
		gc.save
	}
	
	override popState() {
		if (clipBounds != null) {
			gc.restore
			clipBounds = null
		}
		gc.restore
	}
	
	override translate(double xOffset, double yOffset) {
		gc.translate(xOffset, yOffset)
	}
	
	override setForegroundColor(Color foregroundColor) {
		gc.stroke = new javafx.scene.paint.Color(
			foregroundColor.red as double / 255,
			foregroundColor.green as double / 255,
			foregroundColor.blue as double / 255,
			foregroundColor.alpha as double / 255
		)
	}
	
	override setBackgroundColor(Color backgroundColor) {
		gc.fill = new javafx.scene.paint.Color(
			backgroundColor.red as double / 255,
			backgroundColor.green as double / 255,
			backgroundColor.blue as double / 255,
			backgroundColor.alpha as double / 255
		)
	}
	
	override getClipBounds() {
		clipBounds ?: {
			val bounds = gc.canvas.boundsInLocal
			new PixelRectangle(bounds.minX, bounds.minY, bounds.width, bounds.height)
		}
	}
	
	override setClipBounds(PixelRectangle clipBounds) {
//		if (this.clipBounds != null) gc.restore
//		gc.save
//		this.clipBounds = clipBounds
//		gc.beginPath
//		gc.rect(clipBounds.x, clipBounds.y, clipBounds.width, clipBounds.height)
//		gc.clip
	}
	
	override drawLine(double x1, double y1, double x2, double y2) {
		gc.strokeLine(x1, y1, x2, y2)
	}
	
	override drawRectangle(PixelRectangle rect) {
		gc.strokeRect(rect.x.doubleValue, rect.y.doubleValue, rect.width.doubleValue, rect.height.doubleValue)
	}
	
	override fillRectangle(PixelRectangle rect) {
		gc.fillRect(rect.x.doubleValue, rect.y.doubleValue, rect.width.doubleValue, rect.height.doubleValue)
	}
	
	override clearRectangle(PixelRectangle rect) {
		gc.clearRect(rect.x.doubleValue, rect.y.doubleValue, rect.width.doubleValue, rect.height.doubleValue)
	}
	
	override drawText(String text, double x, double y) {
		val fontMetrics = Toolkit::toolkit.fontLoader.getFontMetrics(gc.font)
		gc.strokeText(text, x, y + fontMetrics.ascent)
	}
	
	override calculateTextWidth(String text) {
		val fontMetrics = Toolkit::toolkit.fontLoader.getFontMetrics(gc.font)
		fontMetrics.computeStringWidth(text)
	}
	
	override getFontHeight() {
		val fontMetrics = Toolkit::toolkit.fontLoader.getFontMetrics(gc.font)
		fontMetrics.lineHeight
	}
	
}