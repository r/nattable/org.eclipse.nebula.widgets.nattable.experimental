package org.eclipse.nebula.widgets.nattable.renderer.javafx

import javafx.scene.control.Control
import javax.inject.Inject
import org.eclipse.nebula.widgets.nattable.core.graphics.LayerPainterFactory
import org.eclipse.nebula.widgets.nattable.core.layer.Layer

/**
 * A JavaFX component that renders a NatTable Layer.
 */
class JavaFXNatTable extends Control {
	
	@Inject LayerPainterFactory layerPainterFactory
	
	Layer layer
	
	new() {
		styleClass += "natTable"
	}
	
	override protected getUserAgentStylesheet() {
		class.getResource("natTable.css").toExternalForm
	}
	
	def Layer getLayer() { layer }
	
	def void setLayer(Layer layer) {
		this.layer = layer
	}
	
	def getLayerPainter() { layerPainterFactory.getLayerPainter(layer) }
	
}