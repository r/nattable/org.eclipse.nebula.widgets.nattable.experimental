package org.eclipse.nebula.widgets.nattable.renderer.javafx

import com.sun.javafx.scene.control.behavior.BehaviorBase

class NatTableBehavior extends BehaviorBase<JavaFXNatTable> {
	
	new(JavaFXNatTable control) {
		super(control)
	}
	
}