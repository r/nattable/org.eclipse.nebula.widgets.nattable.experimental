package org.eclipse.nebula.widgets.nattable.renderer.javafx

import com.google.inject.Binder
import com.google.inject.Module
import org.eclipse.nebula.widgets.nattable.core.graphics.LayerPainterFactory
import org.eclipse.nebula.widgets.nattable.core.graphics.DefaultLayerPainterFactory

class JavaFXNatTableModule implements Module {
	
	override configure(Binder binder) {
		binder.bind(typeof(LayerPainterFactory)).toInstance(new DefaultLayerPainterFactory)
	}
	
}