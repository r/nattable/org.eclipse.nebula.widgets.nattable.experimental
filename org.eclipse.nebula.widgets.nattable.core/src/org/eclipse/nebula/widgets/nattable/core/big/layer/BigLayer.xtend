package org.eclipse.nebula.widgets.nattable.core.big.layer

import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.big.layer.axis.BigAxis
import org.eclipse.nebula.widgets.nattable.core.big.layer.cell.BigCell
import org.eclipse.nebula.widgets.nattable.core.command.CommandHandler
import org.eclipse.nebula.widgets.nattable.core.event.EventListener
import org.eclipse.nebula.widgets.nattable.core.event.EventSource

/**
 * A two-dimensional rectangular region of cells.
 * 
 * @see org.eclipse.nebula.widgets.nattable.core.layer.Layer
 */
interface BigLayer extends CommandHandler, EventSource, EventListener {
	
	/**
	 * @return The Axis that characterizes the horizontal dimension of this layer.
	 */
	def BigAxis getHorizontalAxis()
	
	/**
	 * @return The Axis that characterizes the vertical dimension of this layer.
	 */
	def BigAxis getVerticalAxis()
	
	/**
	 * @return The Cell at the given column and row position in this layer.
	 */
	def BigCell getCell(BigInteger columnPosition, BigInteger rowPosition)
	
}