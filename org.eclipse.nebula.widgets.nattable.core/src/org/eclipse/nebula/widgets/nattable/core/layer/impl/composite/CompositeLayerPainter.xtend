package org.eclipse.nebula.widgets.nattable.core.layer.impl.composite

import javax.inject.Inject
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangle
import org.eclipse.nebula.widgets.nattable.core.graphics.GraphicsContext
import org.eclipse.nebula.widgets.nattable.core.graphics.LayerPainterFactory
import org.eclipse.nebula.widgets.nattable.core.layer.LayerPainter

import static extension org.eclipse.nebula.widgets.nattable.core.layer.LayerInvariants.*

class CompositeLayerPainter implements LayerPainter<CompositeLayer> {
	
	@Inject extension LayerPainterFactory
	
	override paintLayer(CompositeLayer compositeLayer, PixelArea layerPaintArea, GraphicsContext gc) {
		var yOffset = 0.0
		for (compositeRowPosition : 0 ..< compositeLayer.rows.size) {
			val compositeRow = compositeLayer.rows.get(compositeRowPosition)
			val isLastCompositeRow = compositeRowPosition == compositeLayer.rows.size - 1
			
			var xOffset = 0.0
			for (compositeColumnPosition : 0 ..< compositeRow.childLayers.size) {
				val childLayer = compositeRow.childLayers.get(compositeColumnPosition)
				val isLastCompositeColumn = compositeColumnPosition == compositeRow.childLayers.size - 1
				
				gc.pushState
				gc.translate(xOffset.doubleValue, yOffset.doubleValue)
				
				val pixelWidth = if (isLastCompositeColumn) layerPaintArea.width - xOffset else childLayer.pixelWidth
				val pixelHeight = if (isLastCompositeRow) layerPaintArea.height - yOffset else childLayer.pixelHeight
				val childLayerPaintArea = new PixelArea(pixelWidth, pixelHeight)
				
				gc.clipBounds = new PixelRectangle(0, 0, pixelWidth, pixelHeight)
				childLayer.layerPainter.paintLayer(childLayer, childLayerPaintArea, gc)
				gc.popState
				
				xOffset = xOffset + childLayer.pixelWidth
			}
			
			yOffset = yOffset + compositeRow.childLayers.get(0).pixelHeight
		}
	}
	
}