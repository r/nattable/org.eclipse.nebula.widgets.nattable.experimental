package org.eclipse.nebula.widgets.nattable.core.layer.axis

/**
 * A set of useful utility functions that calculate invariants that must hold for any Axis.
 */
class AxisInvariants {
	
	def static getPixelSize(Axis axis) {
		axis.getStartPixelOfSegmentPosition(axis.segmentCount) - axis.getStartPixelOfSegmentPosition(0)
	}
	
	def static boolean containsPixelLocation(Axis axis, double pixelLocation) {
		pixelLocation >= 0 && pixelLocation < axis.getStartPixelOfSegmentPosition(axis.segmentCount)
	}
	
	def static boolean containsSegmentPosition(Axis axis, int segmentPosition) {
		segmentPosition >= 0 && segmentPosition < axis.segmentCount
	}

}