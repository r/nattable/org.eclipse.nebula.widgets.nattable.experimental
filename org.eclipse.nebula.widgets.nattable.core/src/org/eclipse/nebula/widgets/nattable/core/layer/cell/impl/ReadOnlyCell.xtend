package org.eclipse.nebula.widgets.nattable.core.layer.cell.impl

import org.eclipse.nebula.widgets.nattable.core.layer.Layer

/**
 * A simple Cell that supports reading but not writing its data value.
 */
class ReadOnlyCell extends AbstractCell {
	
	val Object dataValue
	
	new(Layer layer, int columnPosition, int rowPosition, Object dataValue) {
		super(layer, columnPosition, rowPosition)
		this.dataValue = dataValue
	}
	
	// Cell interface
	
	override getDataValue() { dataValue }
	
	override setDataValue(Object newValue) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
}