package org.eclipse.nebula.widgets.nattable.core.big.layer.axis

import java.io.Serializable
import java.math.BigDecimal
import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.event.EventListener
import org.eclipse.nebula.widgets.nattable.core.event.EventSource

/**
 * Represents a linear dimension (e.g. horizontal, vertical) that is composed of segments (e.g. columns, rows).
 * 
 * @see org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis
 */
interface BigAxis extends EventSource, EventListener {
	
	/**
	 * @return The number of segments on this axis.
	 */
	def BigInteger getSegmentCount()
	
	/**
	 * Gets the start pixel location of the segment at the given position. The start pixel locations of the segments determine the pixel geometry of the axis.
	 * 
	 * This function must be defined for all valid segment position values and <em>also</em> for segment position = segment count.
	 * Technically the segment count is not a valid segment position because segment positions are 0-indexed and range from 0 to segment count - 1.
	 * However, the 'start' pixel location of the segment position after the last segment is used to calculate the pixel size of the last segment,
	 * and also the overall pixel size of the axis.
	 * 
	 * @param segmentPosition
	 * @return The start pixel location of the given segment position.
	 */
	def BigDecimal getStartPixelOfSegmentPosition(BigInteger segmentPosition)
	
	/**
	 * Gets the origin pixel location of the segment at the given position. The is the pixel location to be used when rendering the cell.
	 * This is normally the same as getStartPixelOfSegmentPosition(segmentPosition).
	 * 
	 * @param segmentPosition
	 * @return The origin pixel location of the given segment position.
	 */
	def BigDecimal getOriginPixelOfSegmentPosition(BigInteger segmentPosition)
	
	/**
	 * Gets the pixel size of the segment at the given position. This is the size to be used when rendering the cell.
	 * This is normally the same as getStartPixelOfSegmentPosition(segmentPosition + 1) - getStartPixelOfSegmentPosition(segmentPosition).
	 * 
	 * @param segmentPosition
	 * @return The pixel size of the given segment position.
	 */
	def BigDecimal getPixelSizeOfSegmentPosition(BigInteger segmentPosition)
	
	/**
	 * Gets the position of the segment that is nearest to the the given pixel location.
	 * 
	 * @param pixelLocation
	 * @return The position of the segment that contains the given pixel location.
	 * If the given pixel location is less than 0, the segment position returned will be -1.
	 * If the given pixel location is greater than the size of the axis, then the last segment position + 1 will be returned (= segment count).
	 */
	def BigInteger getSegmentPositionOfPixelLocation(BigDecimal pixelLocation)

	/**
	 * @param segmentPosition
	 * @return The identifier associated with the given segment position.
	 */
	def Serializable getIdOfSegmentPosition(BigInteger segmentPosition)
	
	/**
	 * @param segmentId
	 * @return The position of the segment associated with the given segment identifier.
	 */
	def BigInteger getSegmentPositionOfId(Serializable segmentId)
	
}
