package org.eclipse.nebula.widgets.nattable.core.layer.impl.header

import org.eclipse.nebula.widgets.nattable.core.layer.LayerDataAccessor
import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.eclipse.nebula.widgets.nattable.core.layer.cell.impl.LayerDataAccessorCell
import org.eclipse.nebula.widgets.nattable.core.layer.impl.AbstractLayer

/**
 * A layer of height 1 whose cell data comes from its horizontal axis.
 */
class ColumnHeaderLayer extends AbstractLayer {
	
	Axis horizontalAxis
	val Axis verticalAxis
	LayerDataAccessor layerDataAccessor
	
	new() {
		verticalAxis = new AxisImpl(1, 20)
	}
	
	new(Axis horizontalAxis, LayerDataAccessor layerDataAccessor) {
		this()
		setHorizontalAxis(horizontalAxis)
		setLayerDataAccessor(layerDataAccessor)
	}
	
	def void setHorizontalAxis(Axis horizontalAxis) {
		this.horizontalAxis = horizontalAxis
		this.horizontalAxis.addEventListener(this)
	}
	
	def void setLayerDataAccessor(LayerDataAccessor layerDataAccessor) {
		this.layerDataAccessor = layerDataAccessor
	}
	
	// Layer interface
	
	override getHorizontalAxis() { horizontalAxis }
	override getVerticalAxis() { verticalAxis }
	
	override getCell(int columnPosition, int rowPosition) {
		new LayerDataAccessorCell(this, columnPosition, rowPosition, layerDataAccessor)
	}
	
}