package org.eclipse.nebula.widgets.nattable.core.layer.axis.impl

import org.eclipse.nebula.widgets.nattable.core.event.AbstractEventSourceSink
import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis

abstract class AbstractAxis extends AbstractEventSourceSink implements Axis {

	override getOriginPixelOfSegmentPosition(int segmentPosition) {
		getStartPixelOfSegmentPosition(segmentPosition)
	}
	
	override getPixelSizeOfSegmentPosition(int segmentPosition) {
		getStartPixelOfSegmentPosition(segmentPosition + 1) - getStartPixelOfSegmentPosition(segmentPosition)
	}
	
}