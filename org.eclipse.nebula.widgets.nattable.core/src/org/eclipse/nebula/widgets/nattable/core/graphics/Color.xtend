package org.eclipse.nebula.widgets.nattable.core.graphics

@Data
class Color {
	
	int red
	int green
	int blue
	int alpha
	
	new(int red, int green, int blue) {
		this(red, green, blue, 255)
	}
	
	new(int red, int green, int blue, int alpha) {
		_red = red
		_green = green
		_blue = blue
		_alpha = alpha
	}
	
}