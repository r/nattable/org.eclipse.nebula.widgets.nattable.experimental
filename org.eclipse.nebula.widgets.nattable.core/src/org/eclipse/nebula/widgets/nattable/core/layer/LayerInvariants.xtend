package org.eclipse.nebula.widgets.nattable.core.layer

import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangle

import static extension org.eclipse.nebula.widgets.nattable.core.layer.axis.AxisInvariants.*

/**
 * A set of useful utility functions that calculate invariants that must hold for any Layer.
 */
class LayerInvariants {
	
	// Horizontal axis
	
	def static getColumnCount(Layer layer) {
		layer.horizontalAxis.segmentCount
	}
	
	def static getColumnIdOfPosition(Layer layer, int columnPosition) {
		layer.horizontalAxis.getIdOfSegmentPosition(columnPosition)
	}
	
	def static getStartXPixelOfColumnPosition(Layer layer, int columnPosition) {
		layer.horizontalAxis.getStartPixelOfSegmentPosition(columnPosition)
	}
	
	def static getOriginXPixelOfColumnPosition(Layer layer, int columnPosition) {
		layer.horizontalAxis.getOriginPixelOfSegmentPosition(columnPosition)
	}
	
	def static getPixelWidthOfColumnPosition(Layer layer, int columnPosition) {
		layer.horizontalAxis.getPixelSizeOfSegmentPosition(columnPosition)
	}
	
	def static getPixelWidth(Layer layer) {
		layer.horizontalAxis.pixelSize
	}
	
	def static getColumnPositionOfXPixel(Layer layer, double xPixel) {
		layer.horizontalAxis.getSegmentPositionOfPixelLocation(xPixel)
	}
	
	// Vertical axis
	
	def static getRowCount(Layer layer) {
		layer.verticalAxis.segmentCount
	}
	
	def static getRowIdOfPosition(Layer layer, int rowPosition) {
		layer.verticalAxis.getIdOfSegmentPosition(rowPosition)
	}
	
	def static getStartYPixelOfRowPosition(Layer layer, int rowPosition) {
		layer.verticalAxis.getStartPixelOfSegmentPosition(rowPosition)
	}
	
	def static getOriginYPixelOfRowPosition(Layer layer, int rowPosition) {
		layer.verticalAxis.getOriginPixelOfSegmentPosition(rowPosition)
	}
	
	def static getPixelHeightOfRowPosition(Layer layer, int rowPosition) {
		layer.verticalAxis.getPixelSizeOfSegmentPosition(rowPosition)
	}
	
	def static getPixelHeight(Layer layer) {
		layer.verticalAxis.pixelSize
	}
	
	def static getRowPositionOfYPixel(Layer layer, double yPixel) {
		layer.verticalAxis.getSegmentPositionOfPixelLocation(yPixel)
	}
	
	// Contains
	
	def static boolean containsCellPosition(Layer layer, int columnPosition, int rowPosition) {
		layer.horizontalAxis.containsSegmentPosition(columnPosition) && layer.verticalAxis.containsSegmentPosition(rowPosition)
	}
	
	def static boolean containsPixelLocation(Layer layer, double xPixel, double yPixel) {
		layer.horizontalAxis.containsPixelLocation(xPixel) && layer.verticalAxis.containsPixelLocation(yPixel)
	}
	
	def static PixelRectangle getPixelBounds(Layer layer) {
		new PixelRectangle(
			layer.getStartXPixelOfColumnPosition(0),
			layer.getStartYPixelOfRowPosition(0),
			layer.pixelWidth,
			layer.pixelHeight
		)
	}
	
	def static PixelArea getPixelArea(Layer layer) {
		new PixelArea(
			layer.pixelWidth,
			layer.pixelHeight
		)
	}
	
}
