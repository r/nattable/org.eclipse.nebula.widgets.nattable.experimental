package org.eclipse.nebula.widgets.nattable.core.big.layer.impl

import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.big.layer.BigLayer
import org.eclipse.nebula.widgets.nattable.core.big.layer.axis.BigAxis
import org.eclipse.nebula.widgets.nattable.core.big.layer.cell.impl.BigReadOnlyCell
import org.eclipse.nebula.widgets.nattable.core.command.Command
import org.eclipse.nebula.widgets.nattable.core.event.AbstractEventSourceSink

/**
 * A layer whose cell data values are strings indicating their column and row identifiers.
 */
class BigDummyLayer extends AbstractEventSourceSink implements BigLayer {

	BigAxis horizontalAxis
	BigAxis verticalAxis

	new() {}

	new(BigAxis horizontalAxis, BigAxis verticalAxis) {
		setHorizontalAxis(horizontalAxis)
		setVerticalAxis(verticalAxis)
	}

	def void setHorizontalAxis(BigAxis horizontalAxis) {
		this.horizontalAxis = horizontalAxis
		this.horizontalAxis.addEventListener(this)
	}
	
	def void setVerticalAxis(BigAxis verticalAxis) {
		this.verticalAxis = verticalAxis
		this.verticalAxis.addEventListener(this)
	}
	
	// Layer interface
	
	override getHorizontalAxis() { horizontalAxis }
	override getVerticalAxis() { verticalAxis }
	
	override getCell(BigInteger columnPosition, BigInteger rowPosition) {
		val columnId = horizontalAxis.getIdOfSegmentPosition(columnPosition)
		val rowId = verticalAxis.getIdOfSegmentPosition(rowPosition)
		new BigReadOnlyCell(this, columnPosition, rowPosition, '''Column «columnId», Row «rowId»''')
	}
	
	override doCommand(Command command) {
		// Do nothing
	}
	
}
