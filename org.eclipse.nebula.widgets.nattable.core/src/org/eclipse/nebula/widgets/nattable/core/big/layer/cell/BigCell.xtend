package org.eclipse.nebula.widgets.nattable.core.big.layer.cell

import org.eclipse.nebula.widgets.nattable.core.big.layer.BigLayer
import org.eclipse.nebula.widgets.nattable.core.geometry.BigPositionRectangle

/**
 * Represents a particular cell within a Layer.
 * 
 * @see org.eclipse.nebula.widgets.nattable.core.layer.cell.Cell
 */
interface BigCell {
	
	/**
	 * @return The Layer that this cell belongs to.
	 */
	def BigLayer getLayer()
	
	/**
	 * @return The position bounds of this cell within its layer. These bounds will consist of an origin column and row position
	 * and a column and row span.
	 */
	def BigPositionRectangle getPositionBounds()
	
	/**
	 * @return The data value associated with this cell.
	 */
	def Object getDataValue()
	
	/**
	 * Sets a new value for the data associated with this cell.
	 */
	def void setDataValue(Object newValue)
	
}