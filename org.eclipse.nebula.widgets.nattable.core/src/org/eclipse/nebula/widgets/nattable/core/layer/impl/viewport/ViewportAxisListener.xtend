package org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport

interface ViewportAxisListener {
	
	def void viewportAxisChanged()
	
}