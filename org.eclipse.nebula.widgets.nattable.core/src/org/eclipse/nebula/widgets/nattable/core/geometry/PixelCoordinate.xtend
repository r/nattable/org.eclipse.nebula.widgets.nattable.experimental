package org.eclipse.nebula.widgets.nattable.core.geometry

/**
 * A pixel coordinate (x, y).
 */
@Data
class PixelCoordinate {
	double x
	double y
}