package org.eclipse.nebula.widgets.nattable.core.event

import java.util.LinkedHashSet
import java.util.Set

abstract class AbstractEventSource implements EventSource {
	
	val Set<EventListener> listeners = new LinkedHashSet
	
	// EventSource interface
	
	override addEventListener(EventListener listener) { listeners.add(listener) }
	override removeEventListener(EventListener listener) { listeners.remove(listener) }
	
	//
	
	/**
	 * Pass the event to all the {@link LayerListener}s registered on this layer.
	 */
	def fireEvent(Event event) {
		for (listener : listeners)
			listener.handleEvent(event)
	}
	
}