package org.eclipse.nebula.widgets.nattable.core.geometry

import java.math.BigInteger

/**
 * A layer coordinate position (column, row).
 */
@Data
class BigPositionCoordinate {
	BigInteger columnPosition
	BigInteger rowPosition
}