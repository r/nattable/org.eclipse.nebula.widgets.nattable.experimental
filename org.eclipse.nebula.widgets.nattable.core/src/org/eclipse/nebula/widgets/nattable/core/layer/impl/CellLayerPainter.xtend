package org.eclipse.nebula.widgets.nattable.core.layer.impl

import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangle
import org.eclipse.nebula.widgets.nattable.core.graphics.GraphicsContext
import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.LayerPainter
import org.eclipse.nebula.widgets.nattable.core.layer.cell.Cell
import org.eclipse.nebula.widgets.nattable.core.layer.cell.impl.TextCellPainter

import static java.lang.Math.*

import static extension org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangleInvariants.*
import static extension org.eclipse.nebula.widgets.nattable.core.layer.LayerInvariants.*
import static extension org.eclipse.nebula.widgets.nattable.core.layer.cell.CellInvariants.*

class CellLayerPainter implements LayerPainter<Layer> {
	
	val cellPainter = new TextCellPainter
	
	// LayerPainter interface

	override paintLayer(Layer layer, PixelArea layerPaintArea, GraphicsContext gc) {
		val layerPixelBounds = new PixelRectangle(0, 0, layerPaintArea.width, layerPaintArea.height)
		val clipBounds = gc.clipBounds
		
		// Clear background
		gc.clearRectangle(layerPixelBounds)
		
		val fromColumnPosition = max(layer.getColumnPositionOfXPixel(clipBounds.x), 0)
		val toColumnPosition = min(layer.getColumnPositionOfXPixel(clipBounds.x + clipBounds.width - 1), layer.columnCount - 1)
		val columnRange = toColumnPosition - fromColumnPosition
		
		val fromRowPosition = max(layer.getRowPositionOfYPixel(clipBounds.y), 0)
		val toRowPosition = min(layer.getRowPositionOfYPixel(clipBounds.y + clipBounds.height - 1), layer.rowCount - 1)
		val rowRange = toRowPosition - fromRowPosition
		
		if (columnRange >= 0 && columnRange <= Integer::MAX_VALUE && rowRange >= 0 && rowRange <= Integer::MAX_VALUE) {
			for (columnOffset : 0 .. columnRange)
				for (rowOffset : 0 .. rowRange) {
					val cell = layer.getCell(fromColumnPosition + columnOffset, fromRowPosition + rowOffset)
					
					gc.pushState
					
					val cellPaintBounds = cell.adjustedPaintBounds
					gc.clipBounds = cell.adjustedClipBounds.intersect(layerPixelBounds)
					gc.translate(cellPaintBounds.x.doubleValue, cellPaintBounds.y.doubleValue)
					cellPainter.paintCell(cell, new PixelArea(cellPaintBounds.width, cellPaintBounds.height), gc)
					
					gc.popState
				}
		}
	}
	
	//
	
	def protected getAdjustedPaintBounds(Cell cell) {
		cell.paintBounds
	}
	
	def protected getAdjustedClipBounds(Cell cell) {
		cell.clipBounds
	}
	
}