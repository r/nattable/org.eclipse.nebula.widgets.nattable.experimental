package org.eclipse.nebula.widgets.nattable.core.big.layer.axis.impl

import java.io.Serializable
import java.math.BigDecimal
import java.math.BigInteger

import static extension org.eclipse.nebula.widgets.nattable.core.big.layer.axis.BigAxisInvariants.*

/**
 * A simple Axis implementation.
 */
class BigAxisImpl extends AbstractBigAxis {
	
	BigInteger segmentCount
	BigDecimal defaultSegmentSize
	
	new() {}
	
	new(BigInteger segmentCount, BigDecimal defaultSegmentSize) {
		setSegmentCount(segmentCount)
		setDefaultSegmentSize(defaultSegmentSize)
	}
	
	def void setSegmentCount(BigInteger segmentCount) {
		this.segmentCount = segmentCount
	}
	
	def void setDefaultSegmentSize(BigDecimal defaultSegmentSize) {
		this.defaultSegmentSize = defaultSegmentSize
	}
	
	// Axis interface
	
	override getSegmentCount() {
		segmentCount
	}

	override getStartPixelOfSegmentPosition(BigInteger segmentPosition) {
		if (segmentPosition < BigInteger::ZERO) return -BigDecimal::ONE
		else if (segmentPosition == 0) return BigDecimal::ZERO
		else return new BigDecimal(segmentPosition) * defaultSegmentSize
	}
	
	override getSegmentPositionOfPixelLocation(BigDecimal pixelLocation) {
		if (pixelLocation < BigDecimal::ZERO) return -BigInteger::ONE
		else if (pixelLocation == BigDecimal::ZERO) return BigInteger::ZERO
		else if (pixelLocation >= pixelSize) return segmentCount
		else return (pixelLocation / defaultSegmentSize).toBigInteger
	}
	
	override getIdOfSegmentPosition(BigInteger segmentPosition) {
		segmentPosition
	}
	
	override getSegmentPositionOfId(Serializable segmentId) {
		if (segmentId != null)
			return BigInteger::valueOf((segmentId as Number).longValue)
		else
			return -BigInteger::ONE
	}
	
}