package org.eclipse.nebula.widgets.nattable.core.layer.impl.header

import org.eclipse.nebula.widgets.nattable.core.layer.LayerDataAccessor
import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.eclipse.nebula.widgets.nattable.core.layer.cell.impl.LayerDataAccessorCell
import org.eclipse.nebula.widgets.nattable.core.layer.impl.AbstractLayer

/**
 * A layer of width 1 whose cell data comes from its vertical axis.
 */
class RowHeaderLayer extends AbstractLayer {
	
	val Axis horizontalAxis
	Axis verticalAxis
	LayerDataAccessor layerDataAccessor
	
	new() {
		this.horizontalAxis = new AxisImpl(1, 20)
	}
	
	new(Axis verticalAxis, LayerDataAccessor layerDataAccessor) {
		this()
		setVerticalAxis(verticalAxis)
		setLayerDataAccessor(layerDataAccessor)
	}
	
	def void setVerticalAxis(Axis verticalAxis) {
		this.verticalAxis = verticalAxis
		this.verticalAxis.addEventListener(this)
	}
	
	def void setLayerDataAccessor(LayerDataAccessor layerDataAccessor) {
		this.layerDataAccessor = layerDataAccessor
	}
	
	// Layer interface
	
	override getHorizontalAxis() { horizontalAxis }
	override getVerticalAxis() { verticalAxis }
	
	override getCell(int columnPosition, int rowPosition) {
		new LayerDataAccessorCell(this, columnPosition, rowPosition, layerDataAccessor)
	}
	
}