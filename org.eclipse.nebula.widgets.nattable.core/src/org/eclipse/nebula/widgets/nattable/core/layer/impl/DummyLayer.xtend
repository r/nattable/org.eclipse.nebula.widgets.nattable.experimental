package org.eclipse.nebula.widgets.nattable.core.layer.impl

import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis
import org.eclipse.nebula.widgets.nattable.core.layer.cell.impl.ReadOnlyCell

/**
 * A layer whose cell data values are strings indicating their column and row identifiers.
 */
class DummyLayer extends AbstractLayer {

	Axis horizontalAxis
	Axis verticalAxis

	new() {}

	new(Axis horizontalAxis, Axis verticalAxis) {
		setHorizontalAxis(horizontalAxis)
		setVerticalAxis(verticalAxis)
	}

	def void setHorizontalAxis(Axis horizontalAxis) {
		this.horizontalAxis = horizontalAxis
		this.horizontalAxis.addEventListener(this)
	}
	
	def void setVerticalAxis(Axis verticalAxis) {
		this.verticalAxis = verticalAxis
		this.verticalAxis.addEventListener(this)
	}
	
	// Layer interface
	
	override getHorizontalAxis() { horizontalAxis }
	override getVerticalAxis() { verticalAxis }
	
	override getCell(int columnPosition, int rowPosition) {
		val columnId = horizontalAxis.getIdOfSegmentPosition(columnPosition)
		val rowId = verticalAxis.getIdOfSegmentPosition(rowPosition)
		new ReadOnlyCell(this, columnPosition, rowPosition, '''Column «columnId», Row «rowId»''')
	}
	
}
