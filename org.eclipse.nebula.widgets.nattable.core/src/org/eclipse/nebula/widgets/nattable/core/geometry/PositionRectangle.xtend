package org.eclipse.nebula.widgets.nattable.core.geometry

/**
 * A rectangle in layer position coordinates (column, row).
 */
class PositionRectangle {
	
	val PositionCoordinate positionOrigin
	val int positionWidth
	val int positionHeight
	
	new(int columnPosition, int rowPosition, int positionWidth, int positionHeight) {
		this.positionOrigin = new PositionCoordinate(columnPosition, rowPosition)
		this.positionWidth = positionWidth
		this.positionHeight = positionHeight
	}
	
	def getOriginPosition() { positionOrigin }
	def getColumnPosition() { positionOrigin.columnPosition }
	def getRowPosition() { positionOrigin.rowPosition }
	def getWidth() { positionWidth }
	def getHeight() { positionHeight }
	
	override toString() {
		'''[«class.name» column: «positionOrigin.columnPosition», row: «positionOrigin.rowPosition», width: «positionWidth», height: «positionHeight»]'''
	}
	
}
