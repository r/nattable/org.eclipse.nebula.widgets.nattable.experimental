package org.eclipse.nebula.widgets.nattable.core.graphics

import org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangle

/**
 * Interface for implementing a proxy to a UI toolkit dependent graphics context
 * implementation. The graphics context is at the end the on who paints lines
 * shapes etc. to the canvas to create the NatTable.
 * 
 * e.g. org.eclipse.swt.graphics.GC
 * 
 * @author Dirk Fauth
 *
 */
interface GraphicsContext {

	// State
	
	/**
	 * Saves the current graphics context property state and pushes it onto a stack.
	 */
	def void pushState()
	
	/**
	 * Pops the last saved graphics context state and restores all properties back to what they were when that state was saved.
	 */
	def void popState()

	// Properties
	
	/**
	 * Moves the origin of the current coordinate system by the designated offsets.
	 */
	def void translate(double xOffset, double yOffset)
	
	def void setForegroundColor(Color foregroundColor)
	
	def void setBackgroundColor(Color backgroundColor)
	
	/**
	 * @return The current clipping bounds.
	 */
	def PixelRectangle getClipBounds()
	
	/**
	 * Sets the current clipping bounds.
	 */
	def void setClipBounds(PixelRectangle clipBounds)
	
	// Drawing operations
	
	/**
	 * Draws the specified text at the position specified by x and y coordinates.
	 * Ensure that drawing the text doesn't draw a background and line delimiters 
	 * are handled correctly.
	 * 
	 * @param text The text to draw.
	 * @param x the x coordinate of the top left corner of the rectangular area where the text is to be drawn
	 * @param y the y coordinate of the top left corner of the rectangular area where the text is to be drawn
	 */
	def void drawText(String text, double x, double y)
	
	/**
	 * Draws a line using the foreground color between the points 
	 * (<code>x1</code>, <code>y1</code>) and (<code>x2</code>, <code>y2</code>).
	 *
	 * @param x1 the first point's x coordinate
	 * @param y1 the first point's y coordinate
	 * @param x2 the second point's x coordinate
	 * @param y2 the second point's y coordinate
	 */
	def void drawLine(double x1, double y1, double x2, double y2)
	
	/**
	 * Draws a rectangle with the specified bounds using the foreground color.
	 * 
	 * @param rect The rectangle's pixel bounds.
	 */
	def void drawRectangle(PixelRectangle rect)
	
	/**
	 * Fills the specified rectangular pixel area with the background color.
	 * 
	 * @param rect The rectangle's pixel bounds.
	 */
	def void fillRectangle(PixelRectangle rect)
	
	/**
	 * Clears the specified rectangular pixel area.
	 * 
	 * @param rect The rectangle's pixel bounds.
	 */
	def void clearRectangle(PixelRectangle rect)
	
	/**
	 * Returns the calculated pixel width of the given string if drawn in the current font in the receiver.
	 * Tab expansion and carriage return processing are performed.
	 * <p>
	 *
	 * @param text the text to measure
	 * @return the width in pixels
	 */
	def double calculateTextWidth(String text)
	
	def double getFontHeight()
	
}
