package org.eclipse.nebula.widgets.nattable.core.layer.impl.composite

import org.eclipse.nebula.widgets.nattable.core.geometry.PositionRectangle
import org.eclipse.nebula.widgets.nattable.core.layer.cell.Cell

class CompositeCell implements Cell {
	
	val CompositeLayer compositeLayer
	val Cell subLayerCell
	val int columnPositionOffset
	val int rowPositionOffset
	
	new(CompositeLayer compositeLayer, Cell subLayerCell, int columnPositionOffset, int rowPositionOffset) {
		this.compositeLayer = compositeLayer
		this.subLayerCell = subLayerCell
		this.columnPositionOffset = columnPositionOffset
		this.rowPositionOffset = rowPositionOffset
	}
	
	override getLayer() { compositeLayer }
	
	override getPositionBounds() {
		val subPositionBounds = subLayerCell.positionBounds
		new PositionRectangle(
			columnPositionOffset + subPositionBounds.columnPosition,
			rowPositionOffset + subPositionBounds.rowPosition,
			subPositionBounds.width,
			subPositionBounds.height
		)
	}
	
	override getDataValue() {
		subLayerCell.dataValue
	}
	
	override setDataValue(Object newValue) {
		subLayerCell.dataValue = newValue
	}
	
}