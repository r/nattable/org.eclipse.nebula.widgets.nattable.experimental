package org.eclipse.nebula.widgets.nattable.core.layer.impl.composite

import org.eclipse.nebula.widgets.nattable.core.layer.Layer

/**
 * This interface is used to iterate over a set of sublayers of a CompositeLayer. The CompositeLayer uses this to iterate over all of
 * the sublayers that characterize its horizontal dimension, and has another implementation to iterate over all of the sublayers that
 * characterize its vertical dimension.
 */
interface SubLayerAccessor {
	
	/**
	 * @return An iterable collection of layers.
	 */
	def Iterable<Layer> getSubLayers();
	
}