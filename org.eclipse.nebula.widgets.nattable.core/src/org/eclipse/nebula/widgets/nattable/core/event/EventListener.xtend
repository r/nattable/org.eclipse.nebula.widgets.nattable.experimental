package org.eclipse.nebula.widgets.nattable.core.event

interface EventListener {
	
	def void handleEvent(Event event)
	
}