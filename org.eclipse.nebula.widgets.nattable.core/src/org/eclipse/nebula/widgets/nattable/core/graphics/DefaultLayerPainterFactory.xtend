package org.eclipse.nebula.widgets.nattable.core.graphics

import java.util.HashMap
import javax.inject.Inject
import javax.inject.Provider
import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.LayerPainter
import org.eclipse.nebula.widgets.nattable.core.layer.impl.GridLineCellLayerPainter
import org.eclipse.nebula.widgets.nattable.core.layer.impl.composite.CompositeLayer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.composite.CompositeLayerPainter

class DefaultLayerPainterFactory implements LayerPainterFactory {
	
	@Inject Provider<CompositeLayerPainter> compositeLayerPainterProvider
	@Inject Provider<GridLineCellLayerPainter> gridLineCellLayerPainterProvider
	
	val layerPainterMap = new HashMap<String, LayerPainter<?>>
	
	/**
	 * Gets the associated LayerPainter instance for the given layer. This method will call the createLayerPainter()
	 * method to instantiate LayerPainter instances if they have not already been created; after that the LayerPainter
	 * instance will be cached so that the same instance will be returned for subsequent calls to this method.
	 */
	override final <T extends Layer> getLayerPainter(T layer) {
		var layerPainter = layerPainterMap.get(layer.class.name)
		if (layerPainter == null) {
			layerPainter = layer.createLayerPainter
			layerPainterMap.put(layer.class.name, layerPainter)
		}
		layerPainter as LayerPainter<T>
	}
	
	/**
	 * Creates a LayerPainter for the given layer. This method is called from the DefaultLayerPainter.getLayerPainter()
	 * method, which will cache the created instances. You can override this method to provide specialized layer
	 * painters for particular layers.
	 */
	def protected <T extends Layer> createLayerPainter(T layer) {
		switch (layer) {
			CompositeLayer: compositeLayerPainterProvider.get
			default: gridLineCellLayerPainterProvider.get
		}
	}
	
	
}
