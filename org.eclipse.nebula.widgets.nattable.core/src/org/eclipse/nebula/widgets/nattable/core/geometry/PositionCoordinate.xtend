package org.eclipse.nebula.widgets.nattable.core.geometry

/**
 * A layer coordinate position (column, row).
 */
@Data
class PositionCoordinate {
	int columnPosition
	int rowPosition
}