package org.eclipse.nebula.widgets.nattable.core.layer.impl.composite

@Data
class CompositeLayoutInfo {
	
	int layoutPosition
	int segmentOffset
	
}