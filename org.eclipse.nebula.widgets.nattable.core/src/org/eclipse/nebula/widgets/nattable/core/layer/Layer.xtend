package org.eclipse.nebula.widgets.nattable.core.layer

import org.eclipse.nebula.widgets.nattable.core.command.CommandHandler
import org.eclipse.nebula.widgets.nattable.core.event.EventListener
import org.eclipse.nebula.widgets.nattable.core.event.EventSource
import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis
import org.eclipse.nebula.widgets.nattable.core.layer.cell.Cell

/**
 * A two-dimensional rectangular region of cells.
 */
interface Layer extends CommandHandler, EventSource, EventListener {
	
	/**
	 * @return The Axis that characterizes the horizontal dimension of this layer.
	 */
	def Axis getHorizontalAxis()
	
	/**
	 * @return The Axis that characterizes the vertical dimension of this layer.
	 */
	def Axis getVerticalAxis()
	
	/**
	 * @return The Cell at the given column and row position in this layer.
	 */
	def Cell getCell(int columnPosition, int rowPosition)
	
}