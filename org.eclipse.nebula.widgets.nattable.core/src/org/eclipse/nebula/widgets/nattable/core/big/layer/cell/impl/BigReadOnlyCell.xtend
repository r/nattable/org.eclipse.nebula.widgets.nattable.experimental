package org.eclipse.nebula.widgets.nattable.core.big.layer.cell.impl

import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.big.layer.BigLayer
import org.eclipse.nebula.widgets.nattable.core.big.layer.cell.BigCell
import org.eclipse.nebula.widgets.nattable.core.geometry.BigPositionRectangle

/**
 * A simple Cell that supports reading but not writing its data value.
 */
class BigReadOnlyCell implements BigCell {
	
	val BigLayer layer
	val BigPositionRectangle positionBounds
	val Object dataValue
	
	new(BigLayer layer, BigInteger columnPosition, BigInteger rowPosition, Object dataValue) {
		this.layer = layer
		this.positionBounds = new BigPositionRectangle(columnPosition, rowPosition, BigInteger::ONE, BigInteger::ONE)
		this.dataValue = dataValue
	}
	
	// Cell interface
	
	override getLayer() { layer }
	override getPositionBounds() { positionBounds }
	
	override getDataValue() { dataValue }
	
	override setDataValue(Object newValue) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
}