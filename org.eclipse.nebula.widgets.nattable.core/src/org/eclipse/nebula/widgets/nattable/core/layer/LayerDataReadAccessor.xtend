package org.eclipse.nebula.widgets.nattable.core.layer

import java.io.Serializable

interface LayerDataReadAccessor {
	
	/**
	 * @return The data value associated with the cell at the given column and row position.
	 */
	def Object getDataValueOfCell(Layer layer, Serializable columnId, Serializable rowId)
	
}