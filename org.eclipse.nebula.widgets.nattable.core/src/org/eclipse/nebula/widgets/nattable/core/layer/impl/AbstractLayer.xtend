package org.eclipse.nebula.widgets.nattable.core.layer.impl

import org.eclipse.nebula.widgets.nattable.core.event.AbstractEventSourceSink
import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.command.Command

abstract class AbstractLayer extends AbstractEventSourceSink implements Layer {
	
	override doCommand(Command command) {
		// Do nothing by default
	}
	
}