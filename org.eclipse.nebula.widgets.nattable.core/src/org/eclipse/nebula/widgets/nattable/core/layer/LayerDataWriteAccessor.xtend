package org.eclipse.nebula.widgets.nattable.core.layer

import java.io.Serializable

interface LayerDataWriteAccessor {
	
	/**
	 * Sets a new value for the data associated with the cell at the given column and row position.
	 */
	def void setDataValueOfCell(Object newValue, Layer layer, Serializable columnId, Serializable rowId)
	
}