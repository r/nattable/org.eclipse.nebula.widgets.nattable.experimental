package org.eclipse.nebula.widgets.nattable.core.layer.impl.composite

import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis

/**
 * This interface is used to get an axis from a layer. Particular implementations will select which dimension axis of the layer to return
 * (e.g. return the horizontal axis of the layer).
 */
interface AxisAccessor {
	
	/**
	 * @return An axis associated with the given layer.
	 */
	def Axis getAxis(Layer layer);
	
}