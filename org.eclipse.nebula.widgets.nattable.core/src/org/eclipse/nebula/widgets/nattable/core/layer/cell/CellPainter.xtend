package org.eclipse.nebula.widgets.nattable.core.layer.cell

import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.core.graphics.GraphicsContext

interface CellPainter {
	
	/**
	 * Paints the specified cell.
	 * 
	 * @param cell The cell to paint.
	 * @param cellPaintArea The width and height of the rectangular pixel area to be painted into.
	 * 	 This represents the available area to be painted into, and may be different from the actual cell's area.
	 *   It also may be different from the clipping region of the gc.
	 * @param gc The graphics context to paint into.
	 *   The coordinates of the gc should be offset (translated) to the local coordinate system of the specified cell before being passed in.
	 */
	def void paintCell(Cell cell, PixelArea cellPaintArea, GraphicsContext gc)
	
}