package org.eclipse.nebula.widgets.nattable.core.event

class AbstractEventSourceSink extends AbstractEventSource implements EventListener {
	
	// EventListener interface
	
	override handleEvent(Event event) {
		fireEvent(event)
	}
	
}