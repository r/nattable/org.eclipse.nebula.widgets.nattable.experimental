package org.eclipse.nebula.widgets.nattable.core.layer.impl

import java.io.Serializable
import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.LayerDataAccessor
import org.eclipse.nebula.widgets.nattable.core.layer.LayerDataReadAccessor
import org.eclipse.nebula.widgets.nattable.core.layer.LayerDataWriteAccessor

class LayerDataAccessorImpl implements LayerDataAccessor {
	
	LayerDataReadAccessor layerDataReadAccessor
	LayerDataWriteAccessor layerDataWriteAccessor
	
	new() {}
	
	new(LayerDataReadAccessor layerDataReadAccessor) {
		this(layerDataReadAccessor, null)
	}
	
	new(LayerDataReadAccessor layerDataReadAccessor, LayerDataWriteAccessor layerDataWriteAccessor) {
		setLayerDataReadAccessor(layerDataReadAccessor)
		setLayerDataWriteAccessor(layerDataWriteAccessor)
	}
	
	def void setLayerDataReadAccessor(LayerDataReadAccessor layerDataReadAccessor) {
		this.layerDataReadAccessor = layerDataReadAccessor
	}
	
	def void setLayerDataWriteAccessor(LayerDataWriteAccessor layerDataWriteAccessor) {
		this.layerDataWriteAccessor = layerDataWriteAccessor
	}
	
	override getDataValueOfCell(Layer layer, Serializable columnId, Serializable rowId) {
		layerDataReadAccessor.getDataValueOfCell(layer, columnId, rowId)
	}
	
	override setDataValueOfCell(Object newValue, Layer layer, Serializable columnId, Serializable rowId) {
		layerDataWriteAccessor?.setDataValueOfCell(newValue, layer, columnId, rowId)
	}
	
}