package org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport

import java.io.Serializable
import java.math.BigDecimal
import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.big.layer.axis.BigAxis
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AbstractAxis

import static extension org.eclipse.nebula.widgets.nattable.core.big.layer.axis.BigAxisInvariants.*
import static extension org.eclipse.nebula.widgets.nattable.core.big.math.BigIntegerExtensions.*

class ViewportAxis extends AbstractAxis {
	
	val BigAxis underlyingAxis
	val ViewportAxisListener viewportAxisListener
	
	BigDecimal minPixelLocation = BigDecimal::ZERO
	BigDecimal maxPixelSize
	
	/**
	 * Indicates the visible size of the axis. Note that the pixel locations returned by this ClientDimensionProvider
	 * must be in terms of underlying axis pixel coordinates.
	 */
	BigDecimal visiblePixelSize = -BigDecimal::ONE
	
	BigDecimal viewportOrigin = BigDecimal::ZERO
	
	new(BigAxis underlyingAxis, ViewportAxisListener viewportAxisListener) {
		this.underlyingAxis = underlyingAxis
		this.viewportAxisListener = viewportAxisListener
	}
	
	def getUnderlyingAxis() { underlyingAxis }
	
	// Min pixel location
	
	def setMinPixelLocation(double minPixelLocation) { setMinPixelLocation(new BigDecimal(minPixelLocation)) }
	def setMinPixelLocation(BigDecimal minPixelLocation) { this.minPixelLocation = minPixelLocation }
	
	// Max pixel size
	
	def setMaxPixelSize(double maxPixelSize) { setMaxPixelSize(new BigDecimal(maxPixelSize)) }
	def setMaxPixelSize(BigDecimal maxPixelSize) { this.maxPixelSize = maxPixelSize }
	
	// Visible pixel size
	
	def getVisiblePixelSize() {
		val pixelSize =
			if (visiblePixelSize >= BigDecimal::ZERO) visiblePixelSize
			else underlyingAxis.pixelSize
		
		if (maxPixelSize != null)
			pixelSize.min(maxPixelSize)
		else
			pixelSize
	}
	
	def setVisiblePixelSize(double visiblePixelSize) {
		setVisiblePixelSize(new BigDecimal(visiblePixelSize))
	}
	
	def setVisiblePixelSize(BigDecimal visiblePixelSize) {
		this.visiblePixelSize = visiblePixelSize
	}
	
	// Viewport origin
	
	def getViewportOrigin() { viewportOrigin }
	
	def void setViewportOrigin(double viewportOrigin) {
		setViewportOrigin(new BigDecimal(viewportOrigin))
	}
	
	def void setViewportOrigin(BigDecimal viewportOrigin) {
		this.viewportOrigin = viewportOrigin
		viewportAxisListener?.viewportAxisChanged
	}
	
	def getViewportOriginSegmentPosition() {
		underlyingAxis.getSegmentPositionOfPixelLocation(viewportOrigin)
	}
	
	// Resize
	
	def void handleResize() {
		// If the visible pixel size is greater than the content size, move origin to fill as much content as possible.
		val additionalSize = viewportOrigin + getVisiblePixelSize - underlyingAxis.pixelSize
		if (additionalSize > BigDecimal::ZERO)
			viewportOrigin = BigDecimal::ZERO.max(viewportOrigin - additionalSize)
	}
	
	// Layer interface
	
	override getSegmentCount() {
		val endPixel = viewportOrigin + getVisiblePixelSize
		val endSegmentPosition = underlyingAxis.getSegmentPositionOfPixelLocation(endPixel)
		val isEndPixelInsideEndSegment = endPixel > underlyingAxis.getStartPixelOfSegmentPosition(endSegmentPosition) && endPixel < underlyingAxis.getStartPixelOfSegmentPosition(underlyingAxis.segmentCount)
		val segmentCount = endSegmentPosition - viewportOriginSegmentPosition
		segmentCount.intValue + if (isEndPixelInsideEndSegment) 1 else 0
	}
	
	override getStartPixelOfSegmentPosition(int segmentPosition) {
		if (minPixelLocation != null && segmentPosition == 0)
			minPixelLocation.doubleValue
		else if (maxPixelSize != null && segmentPosition == segmentCount)
			getStartPixelOfSegmentPosition(0) + maxPixelSize
		else
			(underlyingAxis.getStartPixelOfSegmentPosition(viewportOriginSegmentPosition + BigInteger::valueOf(segmentPosition)) - viewportOrigin).doubleValue
	}
	
	override getOriginPixelOfSegmentPosition(int segmentPosition) {
		(underlyingAxis.getOriginPixelOfSegmentPosition(viewportOriginSegmentPosition + BigInteger::valueOf(segmentPosition)) - viewportOrigin).doubleValue
	}
	
	override getPixelSizeOfSegmentPosition(int segmentPosition) {
		if (maxPixelSize != null && segmentPosition == segmentCount - 1)
			maxPixelSize.doubleValue
		else
			underlyingAxis.getPixelSizeOfSegmentPosition(viewportOriginSegmentPosition + BigInteger::valueOf(segmentPosition)).doubleValue
	}
	
	override getSegmentPositionOfPixelLocation(double pixelLocation) {
		val underlyingPixelLocation = viewportOrigin + BigDecimal::valueOf(pixelLocation)
		if (underlyingPixelLocation < BigDecimal::ZERO) return -1
		if (underlyingPixelLocation >= underlyingAxis.pixelSize) return getSegmentCount()
		(underlyingAxis.getSegmentPositionOfPixelLocation(underlyingPixelLocation) - viewportOriginSegmentPosition).intValueExact
	}
	
	override getIdOfSegmentPosition(int segmentPosition) {
		underlyingAxis.getIdOfSegmentPosition(viewportOriginSegmentPosition + BigInteger::valueOf(segmentPosition))
	}
	
	override getSegmentPositionOfId(Serializable segmentId) {
		(underlyingAxis.getSegmentPositionOfId(segmentId) - viewportOriginSegmentPosition).intValueExact
	}
	
}