package org.eclipse.nebula.widgets.nattable.core.layer.impl.composite

import java.io.Serializable
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AbstractAxis

import static extension org.eclipse.nebula.widgets.nattable.core.layer.axis.AxisInvariants.*

/**
 * Represents the axis of a CompositeLayer, which is composed of an amalgamation of the axes of the CompsiteLayer's sublayers.
 */
class CompositeAxis extends AbstractAxis {
	
	val SubLayerAccessor subLayerAccessor
	val extension AxisAccessor axisAccessor
	
	new(SubLayerAccessor subLayerAccessor, AxisAccessor axisAccessor) {
		this.subLayerAccessor = subLayerAccessor
		this.axisAccessor = axisAccessor
	}
	
	def getAxisAccessor() { axisAccessor }
	
	def getSubLayers() {
		subLayerAccessor.subLayers
	}
	
	// Axis interface
	
	override getSegmentCount() {
		subLayers.fold(0, [ acc, layer | acc + layer.axis.segmentCount ])
	}
	
	override getStartPixelOfSegmentPosition(int segmentPosition) {
		var segmentOffset = 0
		var pixelOffset = 0.0
		
		for (subLayer : subLayers) {
			val subAxis = subLayer.axis
			val subSegmentPosition = segmentPosition - segmentOffset
			
			if (subSegmentPosition < subAxis.segmentCount)
				return pixelOffset + subAxis.getStartPixelOfSegmentPosition(subSegmentPosition)
			
			segmentOffset = segmentOffset + subAxis.segmentCount
			pixelOffset = pixelOffset + subAxis.pixelSize
		}
		
		pixelOffset
	}
	
	override getOriginPixelOfSegmentPosition(int segmentPosition) {
		var segmentOffset = 0
		var pixelOffset = 0.0
		
		for (subLayer : subLayers) {
			val subAxis = subLayer.axis
			val subSegmentPosition = segmentPosition - segmentOffset
			
			if (subSegmentPosition < subAxis.segmentCount)
				return pixelOffset + subAxis.getOriginPixelOfSegmentPosition(subSegmentPosition)
			
			segmentOffset = segmentOffset + subAxis.segmentCount
			pixelOffset = pixelOffset + subAxis.pixelSize
		}
		
		pixelOffset
	}
	
	override getPixelSizeOfSegmentPosition(int segmentPosition) {
		var segmentOffset = 0
		
		for (subLayer : subLayers) {
			val subAxis = subLayer.axis
			val subSegmentPosition = segmentPosition - segmentOffset
			
			if (subSegmentPosition < subAxis.segmentCount)
				return subAxis.getPixelSizeOfSegmentPosition(subSegmentPosition)
			
			segmentOffset = segmentOffset + subAxis.segmentCount
		}
		
		0
	}
	
	override getSegmentPositionOfPixelLocation(double pixelLocation) {
		if (pixelLocation < 0) return -1
		if (pixelLocation >= pixelSize) return segmentCount
		
		var segmentOffset = 0
		var pixelOffset = 0.0
		
		for (subLayer : subLayers) {
			val subAxis = subLayer.axis
			val subPixelLocation = pixelLocation - pixelOffset
			
			if (subPixelLocation < subAxis.pixelSize)
				return segmentOffset + subAxis.getSegmentPositionOfPixelLocation(subPixelLocation)
			
			segmentOffset = segmentOffset + subAxis.segmentCount
			pixelOffset = pixelOffset + subAxis.pixelSize
		}
		
		segmentOffset
	}
	
	override getIdOfSegmentPosition(int segmentPosition) {
		var segmentOffset = 0
		
		for (subLayer : subLayers) {
			val subAxis = subLayer.axis
			val subSegmentPosition = segmentPosition - segmentOffset
			
			if (subSegmentPosition < subAxis.segmentCount)
				return subAxis.getIdOfSegmentPosition(subSegmentPosition)
			
			segmentOffset = segmentOffset + subAxis.segmentCount
		}
		
		null
	}
	
	override getSegmentPositionOfId(Serializable segmentId) {
		var segmentOffset = 0
		
		for (subLayer : subLayers) {
			val subAxis = subLayer.axis
			val subSegmentPosition = subAxis.getSegmentPositionOfId(segmentId)
			
			if (subAxis.containsSegmentPosition(subSegmentPosition) && subSegmentPosition >= 0)
				return segmentOffset + subSegmentPosition
			
			segmentOffset = segmentOffset + subAxis.segmentCount
		}
		
		return -1
	}
	
}