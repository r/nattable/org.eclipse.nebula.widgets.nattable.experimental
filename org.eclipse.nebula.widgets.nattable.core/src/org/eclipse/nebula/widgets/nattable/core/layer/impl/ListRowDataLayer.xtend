package org.eclipse.nebula.widgets.nattable.core.layer.impl

import java.util.List
import org.eclipse.nebula.widgets.nattable.core.data.ObjectPropertyAccessor
import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AxisImpl
import org.eclipse.nebula.widgets.nattable.core.layer.cell.impl.ReadOnlyCell

/**
 * A layer whose row data comes from a list.
 */
class ListRowDataLayer extends AbstractLayer {
	
	List<?> list
	
	Axis horizontalAxis
	Axis verticalAxis
	
	extension ObjectPropertyAccessor
	
	new() {}
	
	new(List<?> list, Axis horizontalAxis, ObjectPropertyAccessor objectPropertyAccessor) {
		setList(list)
		setHorizontalAxis(horizontalAxis)
		setObjectPropertyAccessor(objectPropertyAccessor)
	}
	
	def void setList(List<?> list) {
		this.list = list
		this.verticalAxis = new AxisImpl(list.size, 20)
	}
	
	def void setHorizontalAxis(Axis horizontalAxis) {
		this.horizontalAxis = horizontalAxis
		this.horizontalAxis.addEventListener(this)
	}
	
	def void setObjectPropertyAccessor(ObjectPropertyAccessor objectPropertyAccessor) {
		this._objectPropertyAccessor = objectPropertyAccessor
	}
	
	// Layer interface
	
	override getHorizontalAxis() { horizontalAxis }
	override getVerticalAxis() { verticalAxis }
	
	override getCell(int columnPosition, int rowPosition) {
		if (rowPosition <= Integer::MAX_VALUE) {
			val rowObject = list.get(rowPosition)
			val propertyId = horizontalAxis.getIdOfSegmentPosition(columnPosition)
			val value = rowObject.getPropertyValue(propertyId)
			
			new ReadOnlyCell(this, columnPosition, rowPosition, value)
		}
	}
	
}