package org.eclipse.nebula.widgets.nattable.core.layer.cell.impl

import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.core.graphics.GraphicsContext
import org.eclipse.nebula.widgets.nattable.core.layer.cell.Cell
import org.eclipse.nebula.widgets.nattable.core.layer.cell.CellPainter

class TextCellPainter implements CellPainter {
	
	override paintCell(Cell cell, PixelArea cellPaintArea, GraphicsContext gc) {
		val text = cell.dataValue?.toString ?: ""
		
		val centerX = cellPaintArea.width.doubleValue / 2
		val centerY = cellPaintArea.height.doubleValue / 2

		val textWidth = gc.calculateTextWidth(text)

		gc.drawText(
			text,
			centerX - (textWidth / 2),
			centerY - (gc.fontHeight / 2)
		)
	}
	
}