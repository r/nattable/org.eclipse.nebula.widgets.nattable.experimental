package org.eclipse.nebula.widgets.nattable.core.layer.cell

import org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangle

import static extension org.eclipse.nebula.widgets.nattable.core.layer.LayerInvariants.*

/**
 * A set of useful utility functions that calculate invariants that must hold for any Cell.
 */
class CellInvariants {
	
	/**
	 * Gets the paint bounds of the given cell. These are the pixel bounds in which the cell is to be painted.
	 * What portion of this painted area is actually made visible is determined by the clip bounds.
	 * 
	 * @param cell The cell.
	 * @return The paint bounds of the cell relative to its owning layer.
	 * @see #getClipBounds
	 */
	def static getPaintBounds(Cell cell) {
		val layer = cell.layer
		val positionBounds = cell.positionBounds
		
		val startX = layer.getOriginXPixelOfColumnPosition(positionBounds.columnPosition)
		val startY = layer.getOriginYPixelOfRowPosition(positionBounds.rowPosition)
		val width = layer.getPixelWidthOfColumnPosition(positionBounds.columnPosition)
		val height = layer.getPixelHeightOfRowPosition(positionBounds.rowPosition)
		
		new PixelRectangle(startX, startY, width, height)
	}
	
	/**
	 * Gets the clip bounds of the given cell. This describes the rectangular area within which the painted
	 * cell is made visible.
	 * 
	 * @param cell The cell.
	 * @return The clip bounds of the cell relative to its owning layer.
	 * @see #getPaintBounds
	 */
	def static getClipBounds(Cell cell) {
		val layer = cell.layer
		val positionBounds = cell.positionBounds
		
		val startX = layer.getStartXPixelOfColumnPosition(positionBounds.columnPosition)
		val startY = layer.getStartYPixelOfRowPosition(positionBounds.rowPosition)
		val endX = layer.getStartXPixelOfColumnPosition(positionBounds.columnPosition + positionBounds.width)
		val endY = layer.getStartYPixelOfRowPosition(positionBounds.rowPosition + positionBounds.height)
		
		new PixelRectangle(startX, startY, endX - startX, endY - startY)
	}
	
}