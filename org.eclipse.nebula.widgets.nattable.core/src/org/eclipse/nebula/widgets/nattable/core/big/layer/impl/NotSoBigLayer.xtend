package org.eclipse.nebula.widgets.nattable.core.big.layer.impl

import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.big.layer.BigLayer
import org.eclipse.nebula.widgets.nattable.core.big.layer.axis.BigAxis
import org.eclipse.nebula.widgets.nattable.core.big.layer.axis.impl.NotSoBigAxis
import org.eclipse.nebula.widgets.nattable.core.big.layer.cell.impl.NotSoBigCell
import org.eclipse.nebula.widgets.nattable.core.command.Command
import org.eclipse.nebula.widgets.nattable.core.event.Event
import org.eclipse.nebula.widgets.nattable.core.event.EventListener
import org.eclipse.nebula.widgets.nattable.core.layer.Layer

import static extension org.eclipse.nebula.widgets.nattable.core.big.math.BigIntegerExtensions.*

class NotSoBigLayer implements BigLayer {
	
	val Layer layer
	
	BigAxis horizontalAxis
	BigAxis verticalAxis
	
	new(Layer layer) {
		this.layer = layer
	}
	
	override getHorizontalAxis() {
		if (horizontalAxis == null)
			horizontalAxis = new NotSoBigAxis(layer.horizontalAxis)
		horizontalAxis
	}
	
	override getVerticalAxis() {
		if (verticalAxis == null)
			verticalAxis = new NotSoBigAxis(layer.verticalAxis)
		verticalAxis
	}
	
	override getCell(BigInteger columnPosition, BigInteger rowPosition) {
		new NotSoBigCell(layer.getCell(columnPosition.intValueExact, rowPosition.intValueExact))
	}
	
	override doCommand(Command command) {
		layer.doCommand(command)
	}
	
	override addEventListener(EventListener listener) {
		layer.addEventListener(listener)
	}
	
	override removeEventListener(EventListener listener) {
		layer.removeEventListener(listener)
	}
	
	override handleEvent(Event event) {
		layer.handleEvent(event)
	}
	
}