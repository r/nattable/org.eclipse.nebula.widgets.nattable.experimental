package org.eclipse.nebula.widgets.nattable.core.geometry

import java.math.BigInteger

/**
 * A rectangle in layer position coordinates (column, row).
 */
class BigPositionRectangle {
	
	val BigPositionCoordinate positionOrigin
	val BigInteger positionWidth
	val BigInteger positionHeight
	
	new(BigInteger columnPosition, BigInteger rowPosition, BigInteger positionWidth, BigInteger positionHeight) {
		this.positionOrigin = new BigPositionCoordinate(columnPosition, rowPosition)
		this.positionWidth = positionWidth
		this.positionHeight = positionHeight
	}
	
	def getOriginPosition() { positionOrigin }
	def getColumnPosition() { positionOrigin.columnPosition }
	def getRowPosition() { positionOrigin.rowPosition }
	def getWidth() { positionWidth }
	def getHeight() { positionHeight }
	
	override toString() {
		'''[«class.name» column: «positionOrigin.columnPosition», row: «positionOrigin.rowPosition», width: «positionWidth», height: «positionHeight»]'''
	}
	
}
