package org.eclipse.nebula.widgets.nattable.core.command

interface CommandHandler {
	
	def void doCommand(Command command)
	
}