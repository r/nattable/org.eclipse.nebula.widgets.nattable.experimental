package org.eclipse.nebula.widgets.nattable.core.layer.cell

import org.eclipse.nebula.widgets.nattable.core.geometry.PositionRectangle
import org.eclipse.nebula.widgets.nattable.core.layer.Layer

/**
 * Represents a particular cell within a Layer.
 */
interface Cell {
	
	/**
	 * @return The Layer that this cell belongs to.
	 */
	def Layer getLayer()
	
	/**
	 * @return The position bounds of this cell within its layer. These bounds will consist of an origin column and row position
	 * and a column and row span.
	 */
	def PositionRectangle getPositionBounds()
	
	/**
	 * @return The data value associated with this cell.
	 */
	def Object getDataValue()
	
	/**
	 * Sets a new value for the data associated with this cell.
	 */
	def void setDataValue(Object newValue)
	
}