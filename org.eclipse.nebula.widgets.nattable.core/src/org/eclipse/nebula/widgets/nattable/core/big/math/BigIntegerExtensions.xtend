package org.eclipse.nebula.widgets.nattable.core.big.math

import java.math.BigInteger

class BigIntegerExtensions {
	
	/**
	 * Adds the intValueExact method to BigInteger, which is missing in Java <= 1.6. This method will throw an exception
	 * if the BigInteger is larger than what will fit in a 32-bit int. Otherwise the converted primitive int value is returned.
	 */
	def static int intValueExact(BigInteger bigInteger) {
		if (bigInteger > BigInteger::valueOf(Integer::MAX_VALUE))
			throw new IllegalStateException('''Segment count «bigInteger» exceeds integer range''')
		else
			bigInteger.intValue
	}
	
}