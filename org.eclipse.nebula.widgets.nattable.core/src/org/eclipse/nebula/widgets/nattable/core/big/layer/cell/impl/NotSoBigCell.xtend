package org.eclipse.nebula.widgets.nattable.core.big.layer.cell.impl

import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.big.layer.cell.BigCell
import org.eclipse.nebula.widgets.nattable.core.big.layer.impl.NotSoBigLayer
import org.eclipse.nebula.widgets.nattable.core.geometry.BigPositionRectangle
import org.eclipse.nebula.widgets.nattable.core.layer.cell.Cell

class NotSoBigCell implements BigCell {
	
	val Cell cell
	
	new(Cell cell) {
		this.cell = cell
	}
	
	override getLayer() {
		new NotSoBigLayer(cell.layer)
	}
	
	override getPositionBounds() {
		val bounds = cell.positionBounds
		new BigPositionRectangle(BigInteger::valueOf(bounds.columnPosition), BigInteger::valueOf(bounds.rowPosition), BigInteger::valueOf(bounds.width), BigInteger::valueOf(bounds.height))
	}
	
	override getDataValue() {
		cell.dataValue
	}
	
	override setDataValue(Object newValue) {
		cell.dataValue = newValue
	}
	
}