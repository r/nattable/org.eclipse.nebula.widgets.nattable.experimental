package org.eclipse.nebula.widgets.nattable.core.geometry

/**
 * A rectangle in pixel dimensions.
 */
class PixelRectangle {
	val PixelCoordinate pixelOrigin
	val double pixelWidth
	val double pixelHeight
	
	new(double xPixel, double yPixel, double pixelWidth, double pixelHeight) {
		this.pixelOrigin = new PixelCoordinate(xPixel, yPixel)
		this.pixelWidth = pixelWidth
		this.pixelHeight = pixelHeight
	}
	
//	def getOriginPoint() { pixelOrigin }
	def getX() { pixelOrigin.x }
	def getY() { pixelOrigin.y }
	def getWidth() { pixelWidth }
	def getHeight() { pixelHeight }
	
	override toString() {
		'''[«class.name» x: «pixelOrigin.x», y: «pixelOrigin.y», width: «pixelWidth», height: «pixelHeight»]'''
	}
}
