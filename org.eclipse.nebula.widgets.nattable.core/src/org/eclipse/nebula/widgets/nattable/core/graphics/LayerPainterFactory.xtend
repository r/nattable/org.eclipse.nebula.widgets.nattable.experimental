package org.eclipse.nebula.widgets.nattable.core.graphics

import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.LayerPainter

interface LayerPainterFactory {
	
	def <T extends Layer> LayerPainter<T> getLayerPainter(T layer)
	
}