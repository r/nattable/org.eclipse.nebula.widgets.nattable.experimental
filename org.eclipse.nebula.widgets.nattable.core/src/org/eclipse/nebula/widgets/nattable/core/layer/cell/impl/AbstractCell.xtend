package org.eclipse.nebula.widgets.nattable.core.layer.cell.impl

import org.eclipse.nebula.widgets.nattable.core.geometry.PositionRectangle
import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.cell.Cell

/**
 * Useful base class for implementing a Cell.
 */
abstract class AbstractCell implements Cell {
	
	val Layer layer
	val PositionRectangle positionBounds
	
	new(Layer layer, int columnPosition, int rowPosition) {
		this(layer, columnPosition, rowPosition, 1, 1)
	}
	
	new(Layer layer, int columnPosition, int rowPosition, int columnSpan, int rowSpan) {
		this.layer = layer
		this.positionBounds = new PositionRectangle(columnPosition, rowPosition, columnSpan, rowSpan)
	}
	
	// Cell interface
	
	override getLayer() { layer }
	override getPositionBounds() { positionBounds }
	
}