package org.eclipse.nebula.widgets.nattable.core.layer.impl

import org.eclipse.nebula.widgets.nattable.core.layer.LayerDataAccessor
import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis
import org.eclipse.nebula.widgets.nattable.core.layer.cell.impl.LayerDataAccessorCell

class DimensionallyDependentLayer extends AbstractLayer {
	
	Axis horizontalAxis
	Axis verticalAxis
	LayerDataAccessor layerDataAccessor
	
	new() {}
	
	new(Axis horizontalAxis, Axis verticalAxis, LayerDataAccessor layerDataAccessor) {
		setHorizontalAxis(horizontalAxis)
		setVerticalAxis(verticalAxis)
		setLayerDataAccessor(layerDataAccessor)
	}
	
	def void setHorizontalAxis(Axis horizontalAxis) {
		this.horizontalAxis = horizontalAxis
		this.horizontalAxis.addEventListener(this)
	}
	
	def void setVerticalAxis(Axis verticalAxis) {
		this.verticalAxis = verticalAxis
		this.verticalAxis.addEventListener(this)
	}
	
	def void setLayerDataAccessor(LayerDataAccessor layerDataAccessor) {
		this.layerDataAccessor = layerDataAccessor
	}
	
	override getHorizontalAxis() { horizontalAxis }
	override getVerticalAxis() { verticalAxis }
	
	override getCell(int columnPosition, int rowPosition) {
		new LayerDataAccessorCell(this, columnPosition, rowPosition, layerDataAccessor)
	}
	
}