package org.eclipse.nebula.widgets.nattable.core.data

interface ObjectPropertyAccessor {
	
	/**
	 * @return The value of the given property of the object.
	 */
	def Object getPropertyValue(Object obj, Object propertyName)
	
}
