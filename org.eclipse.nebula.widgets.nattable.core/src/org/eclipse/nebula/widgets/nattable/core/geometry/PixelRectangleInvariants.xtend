package org.eclipse.nebula.widgets.nattable.core.geometry

import static java.lang.Math.*

class PixelRectangleInvariants {
	
	def static PixelRectangle intersect(PixelRectangle rect1, PixelRectangle rect2) {
		val startX = max(rect1.x, rect2.x)
		val startY = max(rect1.y, rect2.y)
		val endX = min(rect1.x + rect1.width, rect2.x + rect2.width)
		val endY = min(rect1.y + rect1.height, rect2.y + rect2.height)
		new PixelRectangle(startX, startY, max(0, endX - startX), max(0, endY - startY))
	}
	
	def static boolean isEmpty(PixelRectangle rect) {
		rect.width == 0 || rect.height == 0
	}
	
}