package org.eclipse.nebula.widgets.nattable.core.layer

/**
 * An interface to read and write data associated with a Layer.
 */
interface LayerDataAccessor extends LayerDataReadAccessor, LayerDataWriteAccessor {
}