package org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport

import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.geometry.PositionRectangle
import org.eclipse.nebula.widgets.nattable.core.layer.cell.Cell

import static extension org.eclipse.nebula.widgets.nattable.core.big.math.BigIntegerExtensions.*

class ViewportCell implements Cell {
	
	val ViewportLayer viewportLayer
	val int columnPosition
	val int rowPosition
	
	new(ViewportLayer viewportLayer, int columnPosition, int rowPosition) {
		this.viewportLayer = viewportLayer
		this.columnPosition = columnPosition
		this.rowPosition = rowPosition
	}
	
	override getLayer() { viewportLayer }
	
	override getPositionBounds() {
		val underlyingPositionBounds = underlyingCell.positionBounds
		new PositionRectangle(
			(underlyingPositionBounds.columnPosition - viewportLayer.horizontalAxis.viewportOriginSegmentPosition).intValueExact,
			(underlyingPositionBounds.rowPosition - viewportLayer.verticalAxis.viewportOriginSegmentPosition).intValueExact,
			underlyingPositionBounds.width.intValueExact,
			underlyingPositionBounds.height.intValueExact
		)
	}
	
	override getDataValue() {
		underlyingCell.dataValue
	}
	
	override setDataValue(Object newValue) {
		underlyingCell.dataValue = newValue
	}
	
	def getUnderlyingCell() {
		viewportLayer.underlyingLayer.getCell(
			viewportLayer.horizontalAxis.viewportOriginSegmentPosition + BigInteger::valueOf(columnPosition),
			viewportLayer.verticalAxis.viewportOriginSegmentPosition + BigInteger::valueOf(rowPosition)
		)
	}
	
}