package org.eclipse.nebula.widgets.nattable.core.event

interface EventSource {
	
	def void addEventListener(EventListener listener)

	def void removeEventListener(EventListener listener)
	
}