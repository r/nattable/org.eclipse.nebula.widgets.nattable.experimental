package org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.reorder

import java.io.Serializable
import java.util.List
import java.util.Map
import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis
import org.eclipse.nebula.widgets.nattable.core.layer.axis.impl.AbstractAxis

/**
 * An axis that allows reordering of its segments.
 * <p>
 * NOTE: This axis implementation is not performant for large numbers (e.g. thousands) of segments.
 */
class ReorderAxis extends AbstractAxis {
	
	val List<Serializable> reorderedSegmentIds = newArrayList
	val Map<Integer, Double> segmentPositionToStartPixelMap = newHashMap
	
	Axis underlyingAxis
	
	new() {}
	
	new(Axis underlyingAxis) {
		setUnderlyingAxis(underlyingAxis)
	}
	
	def void setUnderlyingAxis(Axis underlyingAxis) {
		this.underlyingAxis = underlyingAxis
		
		for (segmentPosition : 0 ..< underlyingAxis.segmentCount)
			reorderedSegmentIds += underlyingAxis.getIdOfSegmentPosition(segmentPosition)
	}
	
	// Axis interface
	
	override getSegmentCount() {
		underlyingAxis.segmentCount
	}
	
	override getStartPixelOfSegmentPosition(int segmentPosition) {
		val startPixel = segmentPositionToStartPixelMap.get(segmentPosition)
		if (startPixel != null)
			return startPixel
		else {
			var aggregateSize = 0.0
			
			for (position : 0 ..< segmentPosition) {
				val segmentId = reorderedSegmentIds.get(position)
				val underlyingSegmentPosition = underlyingAxis.getSegmentPositionOfId(segmentId)
				aggregateSize = aggregateSize + underlyingAxis.getPixelSizeOfSegmentPosition(underlyingSegmentPosition)
			}
			
			segmentPositionToStartPixelMap.put(segmentPosition, aggregateSize)
			return aggregateSize
		}
	}
	
	override getSegmentPositionOfPixelLocation(double pixelLocation) {
		if (pixelLocation < 0) return -1
		
		for (segmentPosition : 0 .. segmentCount) {
			val startPixel = getStartPixelOfSegmentPosition(segmentPosition)
			if (startPixel > pixelLocation)
				return segmentPosition - 1
		}
		
		return segmentCount
	}
	
	override getIdOfSegmentPosition(int segmentPosition) {
		reorderedSegmentIds.get(segmentPosition)
	}
	
	override getSegmentPositionOfId(Serializable segmentId) {
		reorderedSegmentIds.indexOf(segmentId)
	}
	
	//
	
	/**
	 * Moves a segment from a given position to a new position.
	 * @param fromSegmentPosition The segment position to move.
	 * @param toSegmentPosition The new position to move the segment to.
	 */
	def void reorderSegmentPosition(int fromSegmentPosition, int toSegmentPosition) {
		val segmentId = reorderedSegmentIds.get(fromSegmentPosition)
		
		reorderedSegmentIds.remove(segmentId)
		reorderedSegmentIds.add(toSegmentPosition, segmentId)
		
		segmentPositionToStartPixelMap.clear
	}
	
}