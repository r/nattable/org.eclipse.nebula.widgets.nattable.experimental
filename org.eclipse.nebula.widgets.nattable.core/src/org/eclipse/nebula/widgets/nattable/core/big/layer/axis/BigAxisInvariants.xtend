package org.eclipse.nebula.widgets.nattable.core.big.layer.axis

import java.math.BigDecimal
import java.math.BigInteger

/**
 * A set of useful utility functions that calculate invariants that must hold for any Axis.
 */
class BigAxisInvariants {
	
	def static getPixelSize(BigAxis axis) {
		axis.getStartPixelOfSegmentPosition(axis.segmentCount) - axis.getStartPixelOfSegmentPosition(BigInteger::ZERO)
	}
	
	def static getPixelSizeOfSegmentPosition(BigAxis axis, BigInteger segmentPosition) {
		axis.getStartPixelOfSegmentPosition(segmentPosition + BigInteger::ONE) - axis.getStartPixelOfSegmentPosition(segmentPosition)
	}
	
	def static boolean containsPixelLocation(BigAxis axis, BigDecimal pixelLocation) {
		pixelLocation >= BigDecimal::ZERO && pixelLocation < axis.getStartPixelOfSegmentPosition(axis.segmentCount)
	}
	
	def static boolean containsSegmentPosition(BigAxis axis, BigInteger segmentPosition) {
		segmentPosition >= BigInteger::ZERO && segmentPosition < axis.segmentCount
	}

}