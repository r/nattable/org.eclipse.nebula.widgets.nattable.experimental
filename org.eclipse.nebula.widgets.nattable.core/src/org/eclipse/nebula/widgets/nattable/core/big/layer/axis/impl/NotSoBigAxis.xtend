package org.eclipse.nebula.widgets.nattable.core.big.layer.axis.impl

import java.io.Serializable
import java.math.BigDecimal
import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.event.Event
import org.eclipse.nebula.widgets.nattable.core.event.EventListener
import org.eclipse.nebula.widgets.nattable.core.layer.axis.Axis

import static extension org.eclipse.nebula.widgets.nattable.core.big.math.BigIntegerExtensions.*

class NotSoBigAxis extends AbstractBigAxis {
	
	val Axis axis
	
	new(Axis axis) {
		this.axis = axis
	}
	
	override getSegmentCount() {
		BigInteger::valueOf(axis.segmentCount)
	}
	
	override getStartPixelOfSegmentPosition(BigInteger segmentPosition) {
		BigDecimal::valueOf(axis.getStartPixelOfSegmentPosition(segmentPosition.intValueExact))
	}
	
	override getSegmentPositionOfPixelLocation(BigDecimal pixelLocation) {
		BigInteger::valueOf(axis.getSegmentPositionOfPixelLocation(pixelLocation.doubleValue))
	}
	
	override getIdOfSegmentPosition(BigInteger segmentPosition) {
		axis.getIdOfSegmentPosition(segmentPosition.intValueExact)
	}
	
	override getSegmentPositionOfId(Serializable segmentId) {
		BigInteger::valueOf(axis.getSegmentPositionOfId(segmentId))
	}
	
	override addEventListener(EventListener listener) {
		axis.addEventListener(listener)
	}
	
	override removeEventListener(EventListener listener) {
		axis.removeEventListener(listener)
	}
	
	override handleEvent(Event event) {
		axis.handleEvent(event)
	}
	
}