package org.eclipse.nebula.widgets.nattable.core.geometry

@Data
class PixelArea {
	
	double width
	double height
	
	new(double width, double height) {
		_width = width
		_height = height
	}
	
}