package org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport

import org.eclipse.nebula.widgets.nattable.core.big.layer.BigLayer
import org.eclipse.nebula.widgets.nattable.core.big.layer.impl.NotSoBigLayer
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangle
import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.impl.AbstractLayer

class ViewportLayer extends AbstractLayer {

	BigLayer underlyingLayer
	ViewportAxis horizontalAxis
	ViewportAxis verticalAxis
	
	boolean horizontalScrollBarVisible = true
	boolean verticalScrollBarVisible = true
	
	new() {}
	
	new(Layer underlyingLayer) {
		this(new NotSoBigLayer(underlyingLayer))
	}
	
	new(BigLayer underlyingLayer) {
		setUnderlyingLayer(underlyingLayer)
	}
	
	// Underlying layer
	
	def BigLayer getUnderlyingLayer() { underlyingLayer }
	
	def void setUnderlyingLayer(Layer underlyingLayer) {
		setUnderlyingLayer(new NotSoBigLayer(underlyingLayer))
	}
	
	def void setUnderlyingLayer(BigLayer underlyingLayer) {
		this.underlyingLayer = underlyingLayer
		this.underlyingLayer.addEventListener(this)
		
		horizontalAxis = new ViewportAxis(underlyingLayer.horizontalAxis, [| fireEvent(new ScrollEvent) ])
		verticalAxis = new ViewportAxis(underlyingLayer.verticalAxis, [| fireEvent(new ScrollEvent) ])
	}
	
	// Axes
	
	def setHorizontalAxis(ViewportAxis horizontalAxis) { this.horizontalAxis = horizontalAxis }
	def setVerticalAxis(ViewportAxis verticalAxis) { this.verticalAxis = verticalAxis }
	
	// Visible pixel rectangle
	
	def void setVisiblePixelRectangle(PixelRectangle visiblePixelRectangle) {
		horizontalAxis.visiblePixelSize = visiblePixelRectangle.width
		verticalAxis.visiblePixelSize = visiblePixelRectangle.height
	}
	
	// Scrollbars
	
	def isHorizontalScrollBarVisible() { horizontalScrollBarVisible }
	def setHorizontalScrollBarVisible(boolean horizontalScrollBarVisible) { this.horizontalScrollBarVisible = horizontalScrollBarVisible }
	
	def isVerticalScrollBarVisible() { verticalScrollBarVisible }
	def setVerticalScrollBarVisible(boolean verticalScrollBarVisible) { this.verticalScrollBarVisible = verticalScrollBarVisible }
	
	// Layer interface
	
	override ViewportAxis getHorizontalAxis() { horizontalAxis }
	override ViewportAxis getVerticalAxis() { verticalAxis }
	
	override getCell(int columnPosition, int rowPosition) {
		new ViewportCell(this, columnPosition, rowPosition)
	}
	
}