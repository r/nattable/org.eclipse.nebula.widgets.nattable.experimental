package org.eclipse.nebula.widgets.nattable.core.layer.impl

import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangle
import org.eclipse.nebula.widgets.nattable.core.graphics.Color
import org.eclipse.nebula.widgets.nattable.core.graphics.GraphicsContext
import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.cell.Cell

import static java.lang.Math.*

import static extension org.eclipse.nebula.widgets.nattable.core.layer.LayerInvariants.*

class GridLineCellLayerPainter extends CellLayerPainter {
	
	// LayerPainter interface
	
	override paintLayer(Layer layer, PixelArea layerPaintArea, GraphicsContext gc) {
		// Draw cells
		super.paintLayer(layer, layerPaintArea, gc)
		
		// Draw grid lines
		drawGridLines(layer, gc)
	}
	
	// CellLayerPainter methods
	
	override protected getAdjustedPaintBounds(Cell cell) {
		val pixelBounds = super.getAdjustedPaintBounds(cell)
		
		new PixelRectangle(
			pixelBounds.x,
			pixelBounds.y,
			pixelBounds.width - 1,
			pixelBounds.height - 1
		)
	}
	
	override protected getAdjustedClipBounds(Cell cell) {
		val pixelBounds = super.getAdjustedClipBounds(cell)
		
		new PixelRectangle(
			pixelBounds.x,
			pixelBounds.y,
			pixelBounds.width - 1,
			pixelBounds.height - 1
		)
	}
	
	//
	
	def drawGridLines(Layer layer, GraphicsContext gc) {
		gc.pushState
		
		gc.foregroundColor = new Color(0, 0, 255)
		
		// Draw
		val clipBounds = gc.clipBounds
		
		val pixelWidth = layer.getPixelWidth.doubleValue
		val pixelHeight = layer.getPixelHeight.doubleValue
		
		// Draw horizontal grid lines
		val fromRowPosition = max(layer.getRowPositionOfYPixel(clipBounds.y) + 1, 1)
		val toRowPosition = min(layer.getRowPositionOfYPixel(clipBounds.y + clipBounds.height), layer.rowCount)
		val rowRange = toRowPosition - fromRowPosition
		if (rowRange >= 0 && rowRange <= Integer::MAX_VALUE)
			for (rowOffset : 0 .. rowRange) {
				val y = (layer.getStartYPixelOfRowPosition(fromRowPosition + rowOffset) - 1).doubleValue
				gc.drawLine(
					0,          y,
					pixelWidth, y
				)
			}
		
		// Draw vertical grid lines
		val fromColumnPosition = max(layer.getColumnPositionOfXPixel(clipBounds.x) + 1, 1)
		val toColumnPosition = min(layer.getColumnPositionOfXPixel(clipBounds.x + clipBounds.width), layer.columnCount)
		val columnRange = toColumnPosition - fromColumnPosition
		if (columnRange >= 0 && columnRange <= Integer::MAX_VALUE)
			for (columnOffset : 0 .. columnRange) {
				val x = (layer.getStartXPixelOfColumnPosition(fromColumnPosition + columnOffset) - 1).doubleValue
				gc.drawLine(
					x, 0,
					x, pixelHeight
				)
			}
		
		gc.popState
	}
	
}