package org.eclipse.nebula.widgets.nattable.core.layer.impl.composite

import java.util.ArrayList
import java.util.List
import org.eclipse.nebula.widgets.nattable.core.layer.Layer

/**
 * Represents a row of sublayers in a CompositeLayer.
 */
class CompositeRow {
	
	val List<Layer> childLayers = new ArrayList<Layer>
	
	/**
	 * Adds a child layer to this layout row.
	 */
	def addChildLayer(Layer childLayer) { childLayers += childLayer }
	
	/**
	 * @return An ordered list of the child layers in this layout row.
	 */
	def getChildLayers() { childLayers }
	
}