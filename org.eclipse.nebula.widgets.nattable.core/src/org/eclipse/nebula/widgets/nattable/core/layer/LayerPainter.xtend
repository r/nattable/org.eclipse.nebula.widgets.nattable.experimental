package org.eclipse.nebula.widgets.nattable.core.layer

import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.core.graphics.GraphicsContext

interface LayerPainter<T extends Layer> {
	
	/**
	 * Paints the specified layer. LayerPainters are typically created by a LayerPainterFactory.
	 * 
	 * @param layer The layer to paint.
	 * @param layerPaintArea The width and height of the rectangular pixel area to be painted into.
	 * 	 This represents the available area to be painted into, and may be different from the actual layer's area.
	 *   It also may be different from the clipping region of the gc.
	 * @param gc The graphics context to paint into.
	 *   The coordinates of the gc should be offset (translated) to the local coordinate system of the specified layer before being passed in.
	 */
	def void paintLayer(T layer, PixelArea layerPaintArea, GraphicsContext gc)
	
}