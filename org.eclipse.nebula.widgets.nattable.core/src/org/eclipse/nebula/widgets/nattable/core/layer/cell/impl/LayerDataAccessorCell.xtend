package org.eclipse.nebula.widgets.nattable.core.layer.cell.impl

import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.LayerDataAccessor

import static extension org.eclipse.nebula.widgets.nattable.core.layer.LayerInvariants.*

/**
 * A Cell that uses a LayerDataAccessor to read and write its data value.
 */
class LayerDataAccessorCell extends AbstractCell {
	
	val LayerDataAccessor layerDataAccessor
	
	new(Layer layer, int columnPosition, int rowPosition, LayerDataAccessor layerDataAccessor) {
		super(layer, columnPosition, rowPosition)
		this.layerDataAccessor = layerDataAccessor
	}
	
	// Cell interface
	
	override getDataValue() {
		layerDataAccessor.getDataValueOfCell(
			layer,
			layer.getColumnIdOfPosition(positionBounds.originPosition.columnPosition),
			layer.getRowIdOfPosition(positionBounds.originPosition.rowPosition)
		)
	}
	
	override setDataValue(Object newValue) {
		layerDataAccessor.setDataValueOfCell(
			newValue,
			layer,
			layer.getColumnIdOfPosition(positionBounds.originPosition.columnPosition),
			layer.getRowIdOfPosition(positionBounds.originPosition.rowPosition)
		)
	}
	
}