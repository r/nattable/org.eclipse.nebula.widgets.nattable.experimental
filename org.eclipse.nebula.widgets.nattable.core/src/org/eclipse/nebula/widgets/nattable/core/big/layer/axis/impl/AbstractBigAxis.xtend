package org.eclipse.nebula.widgets.nattable.core.big.layer.axis.impl

import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.big.layer.axis.BigAxis
import org.eclipse.nebula.widgets.nattable.core.event.AbstractEventSourceSink

abstract class AbstractBigAxis extends AbstractEventSourceSink implements BigAxis {

	override getOriginPixelOfSegmentPosition(BigInteger segmentPosition) {
		getStartPixelOfSegmentPosition(segmentPosition)
	}
	
	override getPixelSizeOfSegmentPosition(BigInteger segmentPosition) {
		getStartPixelOfSegmentPosition(segmentPosition + BigInteger::ONE) - getStartPixelOfSegmentPosition(segmentPosition)
	}
	
}