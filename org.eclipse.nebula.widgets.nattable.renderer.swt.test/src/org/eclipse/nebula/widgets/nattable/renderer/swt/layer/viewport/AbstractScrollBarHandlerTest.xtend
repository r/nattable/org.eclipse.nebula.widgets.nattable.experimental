package org.eclipse.nebula.widgets.nattable.renderer.swt.layer.viewport

import java.math.BigDecimal
import java.math.BigInteger
import org.eclipse.nebula.widgets.nattable.core.big.layer.axis.BigAxis
import org.eclipse.nebula.widgets.nattable.core.big.layer.axis.impl.BigAxisImpl
import org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport.ViewportAxis
import org.eclipse.swt.widgets.ScrollBar
import org.junit.Before

import static org.junit.Assert.*
import static org.mockito.Mockito.*

abstract class AbstractScrollBarHandlerTest {

	BigAxis underlyingAxis
	protected ScrollBar scrollBar
	protected ViewportAxis viewportAxis
	protected ScrollBarHandler scrollBarHandler
	
	new(BigInteger segmentCount, BigDecimal defaultSegmentSize) {
		underlyingAxis = new BigAxisImpl(segmentCount, defaultSegmentSize)
	}
	
	@Before
	def void before() {
		scrollBar = mock(typeof(ScrollBar))
		when(scrollBar.thumb).thenReturn(10)
		when(scrollBar.increment).thenReturn(1)
		when(scrollBar.pageIncrement).thenReturn(10)
		
		viewportAxis = new ViewportAxis(underlyingAxis, null)
		viewportAxis.visiblePixelSize = 500
		
		scrollBarHandler = new ScrollBarHandler(scrollBar, viewportAxis)
		
		assertEquals(BigDecimal::TEN, scrollBarHandler.minThumbSize)
		assertEquals(1, scrollBarHandler.minIncrement)
		assertEquals(10, scrollBarHandler.minPageIncrement)
		
		// ScrollBarHandler constructor makes calls on scrollBar. Reset scrollBar here so they do not interfere with test expectations.
		reset(scrollBar)
	}
	
}