package org.eclipse.nebula.widgets.nattable.renderer.swt.layer.viewport

import java.math.BigDecimal
import java.math.BigInteger
import java.math.MathContext
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.runners.MockitoJUnitRunner

import static org.junit.Assert.*
import static org.mockito.Mockito.*

import static extension org.eclipse.nebula.widgets.nattable.core.big.layer.axis.BigAxisInvariants.*

@RunWith(typeof(MockitoJUnitRunner))
class ScrollBarHandlerRangeLargerThanIntTest extends AbstractScrollBarHandlerTest {

	val static MILLION = 1000 * 1000L
	val static BILLION = 1000 * MILLION
	val static TRILLION = 1000 * BILLION
	val static QUADRILLION = 1000 * TRILLION
	
	//
	
	new() {
		super(BigInteger::valueOf(1 * QUADRILLION), BigDecimal::valueOf(200))
	}
	
	@Test
	def void viewportAtBeginning() {
		scrollBarHandler.recalculateScrollBarSize

		verify(scrollBar).maximum = ScrollBarHandler::MAX_SCROLL_SIZE
		verify(scrollBar).thumb = 10
		verify(scrollBar).increment = 1
		verify(scrollBar).pageIncrement = 10
		verify(scrollBar).selection = 0
	}
	
	@Test
	def void viewportAtEnd() {
		viewportAxis.viewportOrigin = new BigDecimal(200 * QUADRILLION - 1)
		scrollBarHandler.recalculateScrollBarSize

		verify(scrollBar).maximum = ScrollBarHandler::MAX_SCROLL_SIZE
		verify(scrollBar).thumb = 10
		verify(scrollBar).increment = 1
		verify(scrollBar).pageIncrement = 10
		verify(scrollBar).selection = ScrollBarHandler::MAX_SCROLL_SIZE - 10
	}
	
	@Test
	def void moveViewportABit() {
		when(scrollBar.selection).thenReturn(1)
		scrollBarHandler.updateViewportOrigin
		
		val mathContext = new MathContext(15)
		assertEquals(new BigDecimal((1.0 / (ScrollBarHandler::MAX_SCROLL_SIZE - 10)) * (200 * QUADRILLION - 500)).round(mathContext), viewportAxis.viewportOrigin.round(mathContext))
	}
	
	@Test
	def void moveViewportToEnd() {
		when(scrollBar.selection).thenReturn(Integer::MAX_VALUE - 10)
		scrollBarHandler.updateViewportOrigin
		
		assertTrue('''viewport origin «viewportAxis.viewportOrigin» + visible pixel size «viewportAxis.visiblePixelSize» should be >= underlying axis pixel size «viewportAxis.underlyingAxis.pixelSize»''', viewportAxis.viewportOrigin + viewportAxis.visiblePixelSize >= new BigDecimal(200 * QUADRILLION))
	}
	
}