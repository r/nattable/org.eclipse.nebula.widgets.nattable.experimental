package org.eclipse.nebula.widgets.nattable.renderer.swt.layer.viewport

import java.math.BigDecimal
import java.math.BigInteger
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.runners.MockitoJUnitRunner

import static org.junit.Assert.*
import static org.mockito.Mockito.*

import static extension org.eclipse.nebula.widgets.nattable.core.big.layer.axis.BigAxisInvariants.*

@RunWith(typeof(MockitoJUnitRunner))
class ScrollBarHandlerRangeSmallerThanIntTest extends AbstractScrollBarHandlerTest {

	new() {
		super(BigInteger::valueOf(10), BigDecimal::valueOf(200))
	}
	
	@Test
	def void viewportAtBeginning() {
		scrollBarHandler.recalculateScrollBarSize
		
		verify(scrollBar).maximum = 2000  // axis pixel size
		verify(scrollBar).thumb = 500  // visible pixel size
		verify(scrollBar).increment = 125  // 1/4 of visible pixel size
		verify(scrollBar).pageIncrement = 500
		verify(scrollBar).selection = 0
	}
	
	@Test
	def void viewportAtEnd() {
		viewportAxis.viewportOrigin = 1999
		scrollBarHandler.recalculateScrollBarSize
		
		verify(scrollBar).maximum = 2000  // axis pixel size
		verify(scrollBar).thumb = 500  // visible pixel size
		verify(scrollBar).increment = 125  // 1/4 of visible pixel size
		verify(scrollBar).pageIncrement = 500
		verify(scrollBar).selection = 1999
	}
	
	@Test
	def void moveViewportABit() {
		when(scrollBar.selection).thenReturn(1)
		scrollBarHandler.updateViewportOrigin
		
		assertEquals(BigDecimal::ONE, viewportAxis.viewportOrigin)
	}
	
	@Test
	def void moveViewportToEnd() {
		when(scrollBar.selection).thenReturn(1990)
		scrollBarHandler.updateViewportOrigin
		
		assertTrue('''viewport origin «viewportAxis.viewportOrigin» + visible pixel size «viewportAxis.visiblePixelSize» should be >= underlying axis pixel size «viewportAxis.underlyingAxis.pixelSize»''', viewportAxis.viewportOrigin + viewportAxis.visiblePixelSize >= BigDecimal::valueOf(2000))
	}
	
}