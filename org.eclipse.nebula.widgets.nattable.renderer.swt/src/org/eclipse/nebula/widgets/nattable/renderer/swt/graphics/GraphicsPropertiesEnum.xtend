package org.eclipse.nebula.widgets.nattable.renderer.swt.graphics

enum GraphicsPropertiesEnum {
	
	FOREGROUND_COLOR,
	FOREGROUND_ALPHA,
	BACKGROUND_COLOR,
	BACKGROUND_ALPHA,
	CLIP_BOUNDS,
	X_OFFSET,
	Y_OFFSET
	
}