package org.eclipse.nebula.widgets.nattable.renderer.swt

import com.google.inject.Injector
import javax.inject.Inject
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.core.graphics.LayerPainterFactory
import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.renderer.swt.event.mode.Mode
import org.eclipse.nebula.widgets.nattable.renderer.swt.event.mode.ModeSwitcher
import org.eclipse.nebula.widgets.nattable.renderer.swt.event.mode.NormalMode
import org.eclipse.nebula.widgets.nattable.renderer.swt.graphics.SWTGraphicsContext
import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.GC
import org.eclipse.swt.widgets.Canvas
import org.eclipse.swt.widgets.Composite

class SWTNatTable extends Canvas {

	static val int DEFAULT_STYLE_OPTIONS = SWT::NO_BACKGROUND.bitwiseOr(SWT::DOUBLE_BUFFERED)
	
	@Inject extension Injector
	@Inject extension LayerPainterFactory
	
	@Inject ModeSwitcher modeSwitcher

	Layer layer

	new(Composite parent) {
		this(parent, DEFAULT_STYLE_OPTIONS)
	}

	new(Composite parent, int style) {
		super(parent, style)
	}
	
	def Layer getLayer() { layer }
	
	def void setLayer(Layer layer) {
		this.layer = layer
		
		// Event listener
		layer.addEventListener([ event |
			val size = size
			redraw(0, 0, size.x, size.y, true)
		])
		
		// Paint listener
		addPaintListener([ event | paintLayer(event.gc) ])
		
		// Init mode switcher
		modeSwitcher.registerMode(Mode::NORMAL, getInstance(typeof(NormalMode)))
		modeSwitcher.switchToMode(Mode::NORMAL)
		modeSwitcher.listenTo(this)
	}
	
	def private paintLayer(GC swtGC) {
		val paintArea = new PixelArea(bounds.width, bounds.height)
		val gc = new SWTGraphicsContext(swtGC, this, modeSwitcher)
		layer?.layerPainter?.paintLayer(layer, paintArea, gc)
	}
	
}
