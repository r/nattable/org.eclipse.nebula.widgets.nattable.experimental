package org.eclipse.nebula.widgets.nattable.renderer.swt.event.binding

import org.eclipse.nebula.widgets.nattable.renderer.swt.event.action.KeyAction
import org.eclipse.nebula.widgets.nattable.renderer.swt.event.matcher.KeyEventMatcher

@Data
class KeyBinding {

	KeyEventMatcher matcher
	KeyAction action
	
}
