package org.eclipse.nebula.widgets.nattable.renderer.swt.event.mode;

import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;

public interface Mode extends KeyListener, MouseListener, MouseMoveListener, FocusListener {
	
	static final String NORMAL = "NORMAL_MODE";
	
	void setModeSwitcher(ModeSwitcher modeSwitcher);
	
}