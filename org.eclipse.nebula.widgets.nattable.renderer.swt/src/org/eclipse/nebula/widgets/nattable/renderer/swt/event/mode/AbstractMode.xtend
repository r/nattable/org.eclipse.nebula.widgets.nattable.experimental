package org.eclipse.nebula.widgets.nattable.renderer.swt.event.mode

import org.eclipse.swt.events.FocusEvent
import org.eclipse.swt.events.KeyEvent
import org.eclipse.swt.events.MouseEvent

class AbstractMode implements Mode {

	private ModeSwitcher modeSwitcher
	
	override setModeSwitcher(ModeSwitcher modeSwitcher) {
		this.modeSwitcher = modeSwitcher
	}
	
	def protected switchToMode(String modeName) { modeSwitcher.switchToMode(modeName) }
	def protected void switchToMode(Mode mode) { modeSwitcher.switchToMode(mode) }
	
	// Mode interface
	
	override keyPressed(KeyEvent event) {}
	override keyReleased(KeyEvent event) {}
	override mouseDoubleClick(MouseEvent event) {}
	override mouseDown(MouseEvent event) {}
	override mouseUp(MouseEvent event) {}
	override mouseMove(MouseEvent event) {}
	override focusGained(FocusEvent event) {}
	override focusLost(FocusEvent event) {
		switchToMode(Mode::NORMAL)
	}

}
