package org.eclipse.nebula.widgets.nattable.renderer.swt.event.binding

import org.eclipse.nebula.widgets.nattable.renderer.swt.event.action.DragAction
import org.eclipse.nebula.widgets.nattable.renderer.swt.event.matcher.MouseEventMatcher

@Data
class DragBinding {
	
	MouseEventMatcher matcher
	DragAction action
	
}