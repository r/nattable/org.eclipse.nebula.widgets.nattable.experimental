package org.eclipse.nebula.widgets.nattable.renderer.swt.event.action

import org.eclipse.swt.events.MouseEvent

interface MouseAction {
	
	def void run(MouseEvent event)

}
