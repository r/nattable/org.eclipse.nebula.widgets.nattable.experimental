package org.eclipse.nebula.widgets.nattable.renderer.swt.event.action

import org.eclipse.swt.events.KeyEvent

interface KeyAction {
	
	def void run(KeyEvent event)
	
}