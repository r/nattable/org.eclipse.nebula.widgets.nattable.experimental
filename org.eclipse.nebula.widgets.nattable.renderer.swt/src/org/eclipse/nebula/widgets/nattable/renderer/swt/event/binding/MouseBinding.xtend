package org.eclipse.nebula.widgets.nattable.renderer.swt.event.binding

import org.eclipse.nebula.widgets.nattable.renderer.swt.event.action.MouseAction
import org.eclipse.nebula.widgets.nattable.renderer.swt.event.matcher.MouseEventMatcher

@Data
class MouseBinding {

	MouseEventMatcher matcher
	MouseAction action
	
}
