package org.eclipse.nebula.widgets.nattable.renderer.swt.layer.viewport

import java.util.HashMap
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelArea
import org.eclipse.nebula.widgets.nattable.core.geometry.PixelRectangle
import org.eclipse.nebula.widgets.nattable.core.graphics.GraphicsContext
import org.eclipse.nebula.widgets.nattable.core.layer.LayerPainter
import org.eclipse.nebula.widgets.nattable.core.layer.impl.GridLineCellLayerPainter
import org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport.ViewportLayer
import org.eclipse.nebula.widgets.nattable.renderer.swt.graphics.SWTGraphicsContext
import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.Rectangle
import org.eclipse.swt.widgets.Canvas

class SWTViewportLayerPainter implements LayerPainter<ViewportLayer> {
	
	val defaultPainter = new GridLineCellLayerPainter
	
	val viewportLayerToCanvasMap = new HashMap<ViewportLayer, Canvas>
	
	/**
	 * Constructs an SWT Canvas object onto which the ViewportLayer is painted. The same Canvas instance will always be returned
	 * when called with the same ViewportLayer instance. Different ViewportLayer instances will return different Canvas instances.
	 */
	override paintLayer(ViewportLayer viewportLayer, PixelArea layerPaintArea, GraphicsContext gc) {
		viewportLayer.visiblePixelRectangle = new PixelRectangle(0, 0, layerPaintArea.width, layerPaintArea.height)
		
		val swtGC = gc as SWTGraphicsContext
		val viewportCanvas = viewportLayerToCanvasMap.get(viewportLayer) ?: viewportLayer.createViewportCanvas(swtGC)
		
		val clipBounds = swtGC.clipBounds
		viewportCanvas.bounds = new Rectangle((swtGC.getXOffset + clipBounds.x) as int, (swtGC.getYOffset + clipBounds.y) as int, clipBounds.width.intValue, clipBounds.height.intValue)
	}
	
	def private createViewportCanvas(ViewportLayer viewportLayer, SWTGraphicsContext swtGC) {
		// Canvas
		val viewportCanvas = new Canvas(swtGC.SWTComposite, SWT::NO_BACKGROUND.bitwiseOr(SWT::DOUBLE_BUFFERED).bitwiseOr(SWT::H_SCROLL).bitwiseOr(SWT::V_SCROLL))
		viewportLayerToCanvasMap.put(viewportLayer, viewportCanvas)
		swtGC.modeSwitcher.listenTo(viewportCanvas)
		
		// Paint listener
		viewportCanvas.addPaintListener([ event |
			val swtBounds = viewportCanvas.bounds
			defaultPainter.paintLayer(viewportLayer, new PixelArea(swtBounds.width, swtBounds.height), new SWTGraphicsContext(event.gc, viewportCanvas, swtGC.modeSwitcher))
		])
		
		// ScrollBar handlers
		val horizontalScrollBarHandler = new ScrollBarHandler(viewportCanvas.horizontalBar, viewportLayer.horizontalAxis)
		horizontalScrollBarHandler.scrollBarVisible = viewportLayer.horizontalScrollBarVisible
		val verticalScrollBarHandler = new ScrollBarHandler(viewportCanvas.verticalBar, viewportLayer.verticalAxis)
		verticalScrollBarHandler.scrollBarVisible = viewportLayer.verticalScrollBarVisible
		
		viewportCanvas.horizontalBar.addListener(SWT::Selection, horizontalScrollBarHandler)
		viewportCanvas.verticalBar.addListener(SWT::Selection, verticalScrollBarHandler)
		
		viewportCanvas.horizontalBar.addDisposeListener([ viewportCanvas.horizontalBar.removeListener(SWT::Selection, horizontalScrollBarHandler) ])
		viewportCanvas.verticalBar.addDisposeListener([ viewportCanvas.verticalBar.removeListener(SWT::Selection, verticalScrollBarHandler) ])
		
		// Resize listener
		viewportCanvas.addListener(SWT::Resize, [ event |
			// Horizontal
			viewportLayer.horizontalAxis.handleResize
			horizontalScrollBarHandler.recalculateScrollBarSize
			// Vertical
			viewportLayer.verticalAxis.handleResize
			verticalScrollBarHandler.recalculateScrollBarSize
		])
		
		// Dispose listener
		viewportCanvas.addDisposeListener([ viewportLayerToCanvasMap.remove(viewportLayer) ])
		
		viewportCanvas
	}
	
}
