package org.eclipse.nebula.widgets.nattable.renderer.swt.event.matcher

import org.eclipse.swt.events.MouseEvent

interface MouseEventMatcher {
	
	def boolean matches(MouseEvent event)
	
}