package org.eclipse.nebula.widgets.nattable.renderer.swt.event.mode

import org.eclipse.nebula.widgets.nattable.renderer.swt.event.action.DragAction
import org.eclipse.nebula.widgets.nattable.renderer.swt.event.action.MouseAction
import org.eclipse.swt.events.MouseEvent

class MouseMode extends AbstractMode {
	
	MouseEvent initialMouseDownEvent
	MouseAction singleClickAction
	MouseAction doubleClickAction
	boolean mouseDown
	DragAction dragMode
	Runnable singleClickRunnable
	boolean doubleClick
	
	new(MouseEvent initialMouseDownEvent, MouseAction singleClickAction, MouseAction doubleClickAction, DragAction dragMode) {
		mouseDown = true
		
		this.initialMouseDownEvent = initialMouseDownEvent
		
		this.singleClickAction = singleClickAction
		this.doubleClickAction = doubleClickAction
		this.dragMode = dragMode
	}
	
	override mouseUp(MouseEvent event) {
		mouseDown = false
		doubleClick = false
		
		if (singleClickAction != null) {
			//convert/validate/commit/close possible open editor
			//needed in case of conversion/validation errors to cancel any action
//			if (EditUtils.commitAndCloseActiveEditor()) {
				if (doubleClickAction != null) {
					// If a doubleClick action is registered, wait to see if this mouseUp is part of a doubleClick or not.
					singleClickRunnable = [| if (!doubleClick) executeSingleClickAction(singleClickAction, event) ]
					event.display.timerExec(event.display.getDoubleClickTime(), singleClickRunnable)
				} else {
					executeSingleClickAction(singleClickAction, event)
				}
//			}
		} else {
			// No single or double click action registered when mouseUp detected. Switch back to normal mode.
			switchToMode(Mode::NORMAL)
		}
	}
	
	override mouseDoubleClick(MouseEvent event) {
		//double click event is fired after second mouse up event, so it needs to be set to true here
		//this way the SingleClickRunnable knows that it should not execute as a double click was performed
		doubleClick = true
		
		//convert/validate/commit/close possible open editor
		//needed in case of conversion/validation errors to cancel any action
//		if (EditUtils.commitAndCloseActiveEditor()) {
			if (doubleClickAction != null) {
				doubleClickAction.run(event)
				// Double click action complete. Switch back to normal mode.
				switchToMode(Mode::NORMAL)
			}
//		}
	}
	
	// synchronized
	override void mouseMove(MouseEvent event) {
		if (mouseDown && dragMode != null) {
//			if (EditUtils.commitAndCloseActiveEditor()) {
				dragMode.mouseDown(initialMouseDownEvent)
				switchToMode(new DragMode(dragMode))
//			}
//			else {
//				switchToMode(Mode::NORMAL)
//			}
		} else {
			// No drag mode registered when mouseMove detected. Switch back to normal mode.
			switchToMode(Mode::NORMAL)
		}
	}
	
	def private void executeSingleClickAction(MouseAction action, MouseEvent event) {
		//convert/validate/commit/close possible open editor
		//needed in case of conversion/validation errors to cancel any action
//		if (EditUtils.commitAndCloseActiveEditor()) {
			action.run(event)
			// Single click action complete. Switch back to normal mode.
			switchToMode(Mode::NORMAL)
//		}
	}
	
}
