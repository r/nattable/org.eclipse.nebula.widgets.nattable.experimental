package org.eclipse.nebula.widgets.nattable.renderer.swt.event.mode

import org.eclipse.nebula.widgets.nattable.renderer.swt.event.action.DragAction
import org.eclipse.swt.events.MouseEvent

class DragMode extends AbstractMode {

	val DragAction dragAction
	
	new(DragAction dragMode) {
		this.dragAction = dragMode
	}
	
	override mouseMove(MouseEvent event) {
		dragAction.mouseMove(event)
	}
	
	override mouseUp(MouseEvent event) {
		dragAction.mouseUp(event)
		switchToMode(Mode::NORMAL)
	}
	
}
