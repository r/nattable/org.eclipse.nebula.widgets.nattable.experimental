package org.eclipse.nebula.widgets.nattable.renderer.swt

import com.google.inject.Binder
import com.google.inject.Module
import org.eclipse.nebula.widgets.nattable.core.graphics.LayerPainterFactory
import org.eclipse.nebula.widgets.nattable.renderer.swt.graphics.SWTLayerPainterFactory

class SWTNatTableModule implements Module {
	
	override configure(Binder binder) {
		binder.bind(typeof(LayerPainterFactory)).toInstance(new SWTLayerPainterFactory)
	}
	
}