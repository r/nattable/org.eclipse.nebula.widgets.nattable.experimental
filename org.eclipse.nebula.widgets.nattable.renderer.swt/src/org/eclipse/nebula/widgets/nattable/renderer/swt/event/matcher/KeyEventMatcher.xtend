package org.eclipse.nebula.widgets.nattable.renderer.swt.event.matcher

import org.eclipse.swt.events.KeyEvent

interface KeyEventMatcher {
	
	def boolean matches(KeyEvent event)
	
}