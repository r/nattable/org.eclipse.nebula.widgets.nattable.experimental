package org.eclipse.nebula.widgets.nattable.renderer.swt.graphics

import javax.inject.Inject
import javax.inject.Provider
import org.eclipse.nebula.widgets.nattable.core.graphics.DefaultLayerPainterFactory
import org.eclipse.nebula.widgets.nattable.core.layer.Layer
import org.eclipse.nebula.widgets.nattable.core.layer.LayerPainter
import org.eclipse.nebula.widgets.nattable.core.layer.impl.viewport.ViewportLayer
import org.eclipse.nebula.widgets.nattable.renderer.swt.layer.viewport.SWTViewportLayerPainter

class SWTLayerPainterFactory extends DefaultLayerPainterFactory {

	@Inject Provider<SWTViewportLayerPainter> swtViewportLayerPainterProvider

	override <T extends Layer> createLayerPainter(T layer) {
		switch (layer) {
			ViewportLayer: swtViewportLayerPainterProvider.get as LayerPainter<T>
		} ?: super.createLayerPainter(layer)
	}

}
