package org.eclipse.nebula.widgets.nattable.renderer.swt.event.action

import org.eclipse.swt.events.MouseEvent

interface DragAction {
	
	def void mouseDown(MouseEvent event)
	def void mouseMove(MouseEvent event)
	def void mouseUp(MouseEvent event)
	
}