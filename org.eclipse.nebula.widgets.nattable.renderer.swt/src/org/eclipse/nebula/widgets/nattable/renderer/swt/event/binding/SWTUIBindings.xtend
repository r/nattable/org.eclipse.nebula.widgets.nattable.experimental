package org.eclipse.nebula.widgets.nattable.renderer.swt.event.binding

import java.util.LinkedList
import javax.inject.Singleton
import org.eclipse.swt.events.KeyEvent
import org.eclipse.swt.events.MouseEvent

@Singleton
class SWTUIBindings {
	
	val keyBindings = new LinkedList<KeyBinding>
	val mouseMoveBindings = new LinkedList<MouseBinding>
	val mouseDownBindings = new LinkedList<MouseBinding>
	val singleClickBindings = new LinkedList<MouseBinding>
	val doubleClickBindings = new LinkedList<MouseBinding>
	val dragBindings = new LinkedList<DragBinding>
	
	def getKeyBindings() { keyBindings }
	def getMouseMoveBindings() { mouseMoveBindings }
	def getMouseDownBindings() { mouseDownBindings }
	def getSingleClickBindings() { singleClickBindings }
	def getDoubleClickBindings() { doubleClickBindings }
	
	// Lookup /////////////////////////////////////////////////////////////////
	
	def getKeyEventAction(KeyEvent event) {
		keyBindings.findFirst([ matcher.matches(event) ])?.action
	}
	
	def getMouseMoveAction(MouseEvent event) {
		mouseMoveBindings.findFirst([ matcher.matches(event) ])?.action
	}
	
	def getMouseDownAction(MouseEvent event) {
		mouseDownBindings.findFirst([ matcher.matches(event) ])?.action
	}
	
	def getSingleClickAction(MouseEvent event) {
		singleClickBindings.findFirst([ matcher.matches(event) ])?.action
	}
	
	def getDoubleClickAction(MouseEvent event) {
		doubleClickBindings.findFirst([ matcher.matches(event) ])?.action
	}
	
	def getDragAction(MouseEvent event) {
		dragBindings.findFirst([ matcher.matches(event) ])?.action
	}
	
}
