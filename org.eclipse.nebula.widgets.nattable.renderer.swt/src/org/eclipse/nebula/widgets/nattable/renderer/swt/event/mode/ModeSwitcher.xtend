package org.eclipse.nebula.widgets.nattable.renderer.swt.event.mode

import java.util.Map
import org.eclipse.swt.events.FocusEvent
import org.eclipse.swt.events.FocusListener
import org.eclipse.swt.events.KeyEvent
import org.eclipse.swt.events.KeyListener
import org.eclipse.swt.events.MouseEvent
import org.eclipse.swt.events.MouseListener
import org.eclipse.swt.events.MouseMoveListener
import org.eclipse.swt.widgets.Control

/**
 * Modal event handler for NatTable. This class acts as a proxy event listener.
 * It manages a set of IModeEventHandler instances which control the actual
 * event handling for a given mode. This allows the event handling behavior for
 * different modes to be grouped together and isolated from each other.
 */
class ModeSwitcher implements KeyListener, MouseListener, MouseMoveListener, FocusListener {

	val Map<String, Mode> modalEventHandlerMap = newHashMap

	Mode currentMode

	def listenTo(Control control) {
		control.addKeyListener(this)
		control.addMouseListener(this)
		control.addMouseMoveListener(this)
		control.addFocusListener(this)
	}

	/**
	 * Register an event handler to handle events for a given mode.
	 * 
	 * @param mode
	 *            The mode.
	 * @param modeEventHandler
	 *            An IModeEventHandler instance that will handle events in the
	 *            given mode.
	 * 
	 * @see IModeEventHandler
	 */
	def void registerMode(String modeName, Mode mode) {
		modalEventHandlerMap.put(modeName, mode)
	}

	/**
	 * Switch to the given mode.
	 * 
	 * @param mode
	 *            The target mode to switch to.
	 */
	def void switchToMode(String modeName) {
		currentMode = modalEventHandlerMap.get(modeName)
		currentMode.modeSwitcher = this
	}
	
	def void switchToMode(Mode mode) {
		currentMode = mode
		currentMode.modeSwitcher = this
	}

	// KeyListener interface

	override keyPressed(KeyEvent event) { currentMode?.keyPressed(event) }
	override keyReleased(KeyEvent event) { currentMode?.keyReleased(event) }

	// MouseListener interface

	override mouseDoubleClick(MouseEvent event) { currentMode?.mouseDoubleClick(event) }
	override mouseDown(MouseEvent event) { currentMode?.mouseDown(event) }
	override mouseUp(MouseEvent event) { currentMode?.mouseUp(event) }
	
	// MouseMotionListener interface
	
	override mouseMove(MouseEvent event) { currentMode?.mouseMove(event) }

	// FocusListener interface

	override focusGained(FocusEvent event) { currentMode?.focusGained(event) }
	override focusLost(FocusEvent event) { currentMode?.focusLost(event) }

}
