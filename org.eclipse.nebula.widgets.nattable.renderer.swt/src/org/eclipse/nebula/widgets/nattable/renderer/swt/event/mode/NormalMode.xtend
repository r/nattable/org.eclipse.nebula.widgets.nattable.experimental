package org.eclipse.nebula.widgets.nattable.renderer.swt.event.mode

import com.google.inject.Inject
import org.eclipse.nebula.widgets.nattable.renderer.swt.event.binding.SWTUIBindings
import org.eclipse.swt.events.KeyEvent
import org.eclipse.swt.events.MouseEvent
import org.eclipse.swt.widgets.Control

class NormalMode extends AbstractMode {

	@Inject SWTUIBindings uiBindings

	// Event handling /////////////////////////////////////////////////////////
	
	override keyPressed(KeyEvent event) {
		val keyAction = uiBindings.getKeyEventAction(event)
		if (keyAction != null) {
			(event.widget as Control).forceFocus
			keyAction.run(event)
		}
	}
	
	override mouseDown(MouseEvent event) {
//		if (EditUtils.commitAndCloseActiveEditor()) {
			val mouseDownAction = uiBindings.getMouseDownAction(event)
			if (mouseDownAction != null) {
				mouseDownAction.run(event)
			}
			
			val singleClickAction = uiBindings.getSingleClickAction(event)
			val doubleClickAction = uiBindings.getDoubleClickAction(event)
			val dragMode = uiBindings.getDragAction(event)
			
			if (singleClickAction != null || doubleClickAction != null || dragMode != null) {
				switchToMode(new MouseMode(event, singleClickAction, doubleClickAction, dragMode))
			}
//		}
	}

	// syncrhonized
	override mouseMove(MouseEvent event) {
		if (event.x >= 0 && event.y >= 0) {
			val mouseMoveAction = uiBindings.getMouseMoveAction(event)
			if (mouseMoveAction != null) {
				mouseMoveAction.run(event)
			} else {
				(event.widget as Control).setCursor(null)
			}
		}
	}
	
}
