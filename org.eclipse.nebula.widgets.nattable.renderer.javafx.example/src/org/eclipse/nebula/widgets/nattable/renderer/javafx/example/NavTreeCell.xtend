package org.eclipse.nebula.widgets.nattable.renderer.javafx.example

import javafx.scene.control.TreeCell
import org.eclipse.nebula.widgets.nattable.core.example.index.node.IndexNode
import org.eclipse.nebula.widgets.nattable.core.example.index.node.NatExampleNode

class NavTreeCell extends TreeCell<IndexNode> {

	val JavaFXNatExamplesRunner runner
	
	new(JavaFXNatExamplesRunner runner) {
		this.runner = runner
	}
	
	override startEdit() {
		super.startEdit
		
		if (item instanceof NatExampleNode)
			runner.openExampleInTab(item as NatExampleNode)
	}

	override updateItem(IndexNode item, boolean empty) {
		super.updateItem(item, empty)

		if (empty) {
			text = null
			graphic = null
		} else {
			text = string
			graphic = treeItem.graphic
		}
	}

	def private String getString() {
		item?.displayName ?: ""
	}
}
